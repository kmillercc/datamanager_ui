module.exports = function (grunt) {
    /*hh*/
    "use strict";

    var gruntConfig = {
        deployPath1: "C:/OPENTEXT/CS105/",
        deployPath2: "C:/OPENTEXT/CS162/",
        version1: "10_6_0",
        version2: "16_0_6"
    };

    grunt.initConfig({
        gruntConfig: gruntConfig,
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            options: {
                force: true
            },
            init: {
                //src: ["dist/**/*","build/**/*"]
                src: ["dist/","build/"]
            },
            release: ["dist/support/<%= pkg.name %>/css/index.css"]
        },
        uglify: {
            app: {
                options: {
                    sourceMap: true,
                    sourceMapName: "dist/support/<%= pkg.name %>/sourcemap.map"
                },
                files: {
                    "dist/support/<%= pkg.name %>/main.min.js": ['dist/support/<%= pkg.name %>/main.js']
                }
            }
        },
        handlebars: {
            compile: {
                options: {
                    amd: ["handlebars"],
                    namespace: 'App.Templates',
                    processName: function (filePath) {
                        filePath = filePath.replace(/^src\//, '').replace(/\.hbs$/, '');
                        filePath = filePath.replace(/^templates\//, '').replace(/\.hbs$/, '');
                        return filePath;
                    },
                    processPartialName: function (filePath) { // input:  templates/_header.hbs
                        filePath = filePath.replace(/^src\//, '').replace(/\.hbs$/, '');
                        filePath = filePath.replace(/^templates\//, '');
                        filePath = filePath.replace(/_/, '');
                        return filePath;
                    },
                    partialsUseNamespace: true
                },
                files: {
                    "build/templates.js": 'src/templates/**/*.hbs'
                }
            }
        },
        copy: {
            toDist: {
                files: [
                    /* css images */{
                        cwd: "src/css/images",
                        src: "**",
                        dest: "dist/support/<%= pkg.name %>/css/images/",
                        expand: true
                    },
                    {
                        src: ["src/css/adminpage.css"],
                        dest: "dist/support/<%= pkg.name %>/css/adminpage.css"
                    },
                    /* tabletools swf files what is this?*/
                    {
                        cwd: "bower_components/datatables-tabletools/swf/",
                        src: "*.swf",
                        dest: "dist/support/<%= pkg.name %>/images/",
                        expand: true
                    },
                    {
                        cwd: "bower_components/bootstrap/img",
                        src: "**",
                        dest: "dist/support/<%= pkg.name %>/css/images/",
                        expand: true
                    },
                    {
                        cwd: "shims/dynatree/1.2.6/skin",
                        src: "*.gif",
                        dest: "dist/support/<%= pkg.name %>/css/images/",
                        expand: true
                    },
                    {
                        cwd: "bower_components/jquery-ui/themes/smoothness/images",
                        src: "**",
                        dest: "dist/support/<%= pkg.name %>/css/images/",
                        expand: true
                    },
                    {
                        cwd: "shims/liteaccordion/2.2.0/css",
                        src: "*.png",
                        dest: "dist/support/<%= pkg.name %>/images/",
                        expand: true
                    },
                    {
                        cwd: "bower_components/datatables/media/images",
                        src: "**",
                        dest: "dist/support/<%= pkg.name %>/images/",
                        expand: true
                    },
                    /*images*/{
                        cwd: "src/images/",
                        src: "**",
                        dest: "dist/support/<%= pkg.name %>/images/",
                        expand: true
                    },
                    /*html 1*/ {
                        cwd: "src/html/",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version1 %>/module/<%= pkg.name %>_<%= gruntConfig.version1 %>/html/",
                        expand: true
                    },
                    /*html*/ {
                        cwd: "src/html/",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version2 %>/module/<%= pkg.name %>_<%= gruntConfig.version2 %>/html/",
                        expand: true
                    }
                ]
            },
            supportToMod: {
                files: [
                    /*support files to support in version specific folder 1*/{
                        cwd: "dist/support/",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version1 %>/support/",
                        expand: true
                    },
                    /*support files to support in version specific folder 2*/{
                        cwd: "dist/support/",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version2 %>/support/",
                        expand: true
                    },
                    /*support files to mod 1*/{
                        cwd: "dist/support/<%= pkg.name %>",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version1 %>/module/<%= pkg.name %>_<%= gruntConfig.version1 %>/support/",
                        expand: true
                    },
                    /*support files to mod 2*/{
                        cwd: "dist/support/<%= pkg.name %>",
                        src: "**",
                        dest: "dist/<%= gruntConfig.version2 %>/module/<%= pkg.name %>_<%= gruntConfig.version2 %>/support/",
                        expand: true
                    }
                ]
            },
            toLivelink: {
                files: [
                    /*copy to OTHOME*/
                    {cwd: "dist/<%= gruntConfig.version1 %>/", src: "**", dest: "<%= gruntConfig.deployPath1 %>", expand: true},
                    {cwd: "dist/<%= gruntConfig.version2 %>/", src: "**", dest: "<%= gruntConfig.deployPath2 %>", expand: true}
                ]
            }
        },
        concat: {
            css: {
                src: ["bower_components/bootstrap/docs/assets/css/bootstrap.css",
                    "bower_components/jquery-ui/themes/smoothness/jquery-ui.css",
                    "shims/dynatree/1.2.6/skin/ui.dynatree.css",
                    "bower_components/datatables/media/css/demo_table_jui.css",
                    "bower_components/datatables-tabletools/css/dataTables.tableTools.css",
                    "shims/datatables-editor/dataTables.editor.css",
                    "bower_components/datatables-scroller/css/dataTables.scroller.min.css",
                    "bower_components/datatables-fixedcolumns/css/dataTables.fixedColumns.css",
                    "src/css/customfile.css",
                    "shims/liteaccordion/2.2.0/css/liteaccordion.css",
                    "src/css/app.css",
                    "src/css/overrides.css"],
                dest: 'dist/support/<%= pkg.name %>/css/index.css'
            }
        },
        requirejs: {
            app: {
                options: {
                    insertRequire: ['main'],
                    name: "../../bower_components/almond/almond",
                    out: "dist/support/<%= pkg.name %>/main.js",
                    wrapShim: true,
                    optimize: "none",
                    baseUrl: "src/js/",
                    logLevel: 1,
                    include: ["main",
                        "app",
                        "templates",
                        "blank",
                        "LoadValidateController",
                        "BulkLoaderPageController",
                        "CsvPageController",
                        "LoadFromCsvPage",
                        "GenerateCsvPage",
                        "LoadUsersPage",
                        "LoadFromFileStructurePage",
                        "ManageLoadPathsPage",
                        "ManageLoadJobsPage",
                        "ManageLoadPrivilegesPage",
                        "ManageLoadProfilesPage",
                        "BL_SettingsPage",
                        "LoadPathModel",
                        "LoadJobModel",
                        "LoadPrivilegeModel",
                        "LoadProfileModel",
                        "ExportController",
                        "BulkExporterPageController",
                        "ExportUsersGroupsPage",
                        "ExportMetadataPage",
                        "ExportObjectsPage",
                        "ManageExportPathsPage",
                        "ManageExportJobsPage",
                        "ManageExportPrivilegesPage",
                        "ManageExportProfilesPage",
                        "BE_SettingsPage",
                        "ExportPathModel",
                        "ExportJobModel",
                        "ExportPrivilegeModel",
                        "ExportProfileModel",
                        "ErrorController",
                        "ProcessController",
                        "RootPageController",
                        "FaqPage",
                        "HelpPage",
                        "AbstractDataTableController",
                        "DataTableCrudController",
                        "DataTableCsvController",
                        "ContentServerAPI",
                        "TreeController",
                        "JobScheduleController",
                        "ProfileController",
                        "popup",
                        "ProgressBar",
                        "customfile",
                        "OScriptParser",
                        "StopWatch",
                        "combobox",
                        "DataTableUtils",
                        "ManageDaJobsPage",
                        "ConvertDropoffsPage",
                        "DaJobModel",
                        "IdNameModel",
                        "TemplateUtils",
                        "Tester",
                        /* labels */
                        "bt_lbl_managedajobs",
                        "bt_lbl_convertdropoffs",
                        "be_lbl_exportfromdataids",
                        "be_lbl_exportmetadata",
                        "be_lbl_exportobjects",
                        "be_lbl_exportusers",
                        "be_lbl_faq",
                        "be_lbl_help",
                        "be_lbl_manageexportjobs",
                        "be_lbl_manageexportpaths",
                        "be_lbl_manageprivileges",
                        "be_lbl_manageprofiles",
                        "be_lbl_settings",
                        "lbl_scheduleJob",
                        "bl_lbl_faq",
                        "bl_lbl_help",
                        "bl_lbl_loadcsv",
                        "bl_lbl_generatecsv",
                        "bl_lbl_loadfilestructure",
                        "bl_lbl_loadusers",
                        "bl_lbl_manageloadjobs",
                        "bl_lbl_manageloadpaths",
                        "bl_lbl_manageprivileges",
                        "bl_lbl_manageprofiles",
                        "bl_lbl_settings",
                        /*../libs*/
                        'jquery',
                        'dynatree',
                        'contextMenu',
                        'async',
                        'routie',
                        'handlebars',
                        'jquery-ui',
                        'bootstrap',
                        'bootbox',
                        'lodash',
                        'liteAccordion',
                        'datatables',
                        'datatablesEditor',
                        'scroller',
                        'fixedColumns',
                        'tabletools',
                        'zeroclipboard'],
                    paths: {
                        "main": "main",
                        "app": "app",
                        "blank": "utils/blank",
                        "templates": "../../build/templates",
                        "router": "router",
                        "LoadValidateController": "bl/controllers/LoadValidateController",
                        "BulkLoaderPageController": "bl/controllers/BulkLoaderPageController",
                        "CsvPageController": "bl/controllers/CsvPageController",
                        "LoadFromCsvPage": "bl/controllers/LoadFromCsvPage",
                        "GenerateCsvPage": "bl/controllers/GenerateCsvPage",
                        "LoadUsersPage": "bl/controllers/LoadUsersPage",
                        "LoadFromFileStructurePage": "bl/controllers/LoadFromFileStructurePage",
                        "ManageLoadPathsPage": "bl/controllers/ManageLoadPathsPage",
                        "ManageLoadJobsPage": "bl/controllers/ManageLoadJobsPage",
                        "ManageLoadPrivilegesPage": "bl/controllers/ManageLoadPrivilegesPage",
                        "ManageLoadProfilesPage": "bl/controllers/ManageLoadProfilesPage",
                        "BL_SettingsPage": "bl/controllers/SettingsPage",
                        "LoadPathModel": "bl/models/loadpath",
                        "LoadJobModel": "bl/models/loadjob",
                        "LoadPrivilegeModel": "bl/models/loadprivilege",
                        "LoadProfileModel": "bl/models/loadprofile",
                        "ExportController": "be/controllers/ExportController",
                        "BulkExporterPageController": "be/controllers/BulkExporterPageController",
                        "ExportUsersGroupsPage": "be/controllers/ExportUsersGroupsPage",
                        "ExportMetadataPage": "be/controllers/ExportMetadataPage",
                        "ExportObjectsPage": "be/controllers/ExportObjectsPage",
                        "ManageExportPathsPage": "be/controllers/ManageExportPathsPage",
                        "ManageExportJobsPage": "be/controllers/ManageExportJobsPage",
                        "ManageExportPrivilegesPage": "be/controllers/ManageExportPrivilegesPage",
                        "ManageExportProfilesPage": "be/controllers/ManageExportProfilesPage",
                        "BE_SettingsPage": "be/controllers/SettingsPage",
                        "ExportPathModel": "be/models/exportpath",
                        "ExportJobModel": "be/models/exportjob",
                        "ExportPrivilegeModel": "be/models/exportprivilege",
                        "ExportProfileModel": "be/models/exportprofile",
                        "ErrorController": "controllers/ErrorController",
                        "ProcessController": "controllers/ProcessController",
                        "RootPageController": "controllers/RootPageController",
                        "FaqPage": "controllers/FaqPage",
                        "HelpPage": "controllers/HelpPage",
                        "AbstractDataTableController": "controllers/AbstractDataTableController",
                        "DataTableCrudController": "controllers/DataTableCrudController",
                        "DataTableCsvController": "controllers/DataTableCsvController",
                        "ContentServerAPI": "utils/ContentServerAPI",
                        "TreeController": "controllers/TreeController",
                        "JobScheduleController": "controllers/JobScheduleController",
                        "ProfileController": "controllers/ProfileController",
                        "popup": "utils/popup",
                        "ProgressBar": "utils/ProgressBar",
                        "customfile": "utils/customfile",
                        "OScriptParser": "utils/OScriptParser",
                        "StopWatch": "utils/StopWatch",
                        "combobox": "utils/combobox",
                        "DataTableUtils": "utils/DataTableUtils",
                        "ManageDaJobsPage": "bt/controllers/ManageDaJobsPage",
                        "ConvertDropoffsPage": "bt/controllers/ConvertDropoffsPage",
                        "DaJobModel": "bt/models/dajob",
                        "DropoffJobModel": "bt/models/dropoffjob",
                        "IdNameModel": "be/models/idname",
                        "TemplateUtils": "utils/TemplateUtils",
                        "Tester": "utils/Tester",
                        /* labels */
                        "bt_lbl_convertdropoffs" : "bt/labels/convertdropoffs",
                        "bt_lbl_managedajobs": "bt/labels/managedajobs",
                        "be_lbl_exportfromdataids" : "be/labels/exportfromdataids",
                        "be_lbl_exportmetadata": "be/labels/exportmetadata",
                        "be_lbl_exportobjects": "be/labels/exportobjects",
                        "be_lbl_exportusers": "be/labels/exportusers",
                        "be_lbl_faq": "be/labels/faq",
                        "be_lbl_help": "be/labels/help",
                        "be_lbl_manageexportjobs": "be/labels/manageexportjobs",
                        "be_lbl_manageexportpaths": "be/labels/manageexportpaths",
                        "be_lbl_manageprivileges": "be/labels/manageprivileges",
                        "be_lbl_manageprofiles": "be/labels/manageprofiles",
                        "be_lbl_settings": "be/labels/settings",
                        "lbl_scheduleJob": "labels/scheduleJob",
                        "bl_lbl_faq": "bl/labels/faq",
                        "bl_lbl_help": "bl/labels/help",
                        "bl_lbl_loadcsv": "bl/labels/loadcsv",
                        "bl_lbl_generatecsv": "bl/labels/generatecsv",
                        "bl_lbl_loadfilestructure": "bl/labels/loadfilestructure",
                        "bl_lbl_loadusers": "bl/labels/loadusers",
                        "bl_lbl_manageloadjobs": "bl/labels/manageloadjobs",
                        "bl_lbl_manageloadpaths": "bl/labels/manageloadpaths",
                        "bl_lbl_manageprivileges": "bl/labels/manageprivileges",
                        "bl_lbl_manageprofiles": "bl/labels/manageprofiles",
                        "bl_lbl_settings": "bl/labels/settings",
                        /*libs*/
                        'jquery': '../../bower_components/jquery/jquery',
                        'dynatree': '../../shims/dynatree/1.2.6/jquery.dynatree',
                        /*'contextMenu': '../../bower_components/jQuery-contextMenu/src/jquery.contextMenu',*/
                        /* have to use older version for now because new version does not work with my code*/
                        'contextMenu': '../../shims/jQuery-contextMenu/jquery.ui-contextmenu.min',
                        'async': '../../node_modules/async/lib/async',
                        'routie': '../../bower_components/routie/dist/routie',
                        'handlebars': '../../node_modules/grunt-contrib-handlebars/node_modules/handlebars/dist/handlebars.runtime',
                        'jquery-ui': '../../bower_components/jquery-ui/ui/jquery-ui',
                        'bootstrap': '../../bower_components/bootstrap/docs/assets/js/bootstrap',
                        'bootbox': '../../bower_components/bootbox/bootbox',
                        'lodash': '../../bower_components/lodash/lodash',
                        'liteAccordion': '../../shims/liteaccordion/2.2.0/js/liteaccordion.jquery.min',
                        'datatables': '../../bower_components/datatables/media/js/jquery.dataTables',
                        'datatablesEditor': '../../shims/datatables-editor/dataTables.editor',
                        'scroller': '../../bower_components/datatables-scroller/js/dataTables.scroller',
                        'fixedColumns': '../../bower_components/datatables-fixedcolumns/js/dataTables.fixedColumns',
                        'tabletools': '../../bower_components/datatables-tabletools/js/dataTables.tableTools',
                        'zeroclipboard': '../../bower_components/zeroclipboard/dist/ZeroClipboard'
                    },
                    shim: {
                        datatablesEditor: {
                            deps: ['datatables', 'tabletools']
                        },
                        liteAccordion: {
                            deps: ['jquery']
                        },
                        'dynatree': {
                            deps: ['jquery', 'jquery-ui']
                        },
                        'async': {
                            exports: 'async'
                        },
                        'jquery': {
                            deps: []
                        },
                        'handlebars': {
                            exports: 'Handlebars'
                        },
                        routie: {
                            exports: 'routie'
                        },
                        StopWatch: {
                            exports: 'StopWatch'
                        },
                        'bootbox': {
                            deps: ['jquery', 'bootstrap'],
                            exports: 'bootbox'
                        },
                        'contextMenu': {
                            deps: ['jquery', 'jquery-ui']
                        },
                        'jquery-ui': {
                            deps: ['jquery']
                        },
                        'datatables': {
                            deps: ['jquery'],
                            exports: 'DataTable'
                        },
                        'combobox': {
                            deps: ['jquery-ui']
                        },
                        customfile: {
                            deps: ['jquery']
                        },
                        tabletools: {
                            deps: ['jquery', 'datatables'],
                            exports: 'TableTools'
                        },
                        scroller: {
                            deps: ['jquery', 'datatables'],
                            exports: "scroller"
                        },
                        fixedHeader: {
                            deps: ['jquery', 'datatables'],
                            exports: 'fixedHeader'
                        },
                        fixedColumns: {
                            deps: ['jquery', 'datatables'],
                            exports: 'fixedColumns'
                        }
                    }
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'dist/support/<%= pkg.name %>/css/index.min.css': ['dist/support/<%= pkg.name %>/css/index.css']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'js/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        replace: {
            indexHtml1: {
                src: ['dist/module/<%= gruntConfig.version1 %>/<%= pkg.name %>_<%= gruntConfig.version %>/html/index.html'],
                overwrite: true,                 // overwrite matched source files
                replacements: [
                    {
                        from: "index.css",
                        to: "index.min.css"
                    },
                    {
                        from: "main.js",
                        to: "main.min.js"
                    }
                ]
            },
            indexHtml2: {
                src: ['dist/module/<%= gruntConfig.version2 %>/<%= pkg.name %>_<%= gruntConfig.version %>/html/index.html'],
                overwrite: true,                 // overwrite matched source files
                replacements: [
                    {
                        from: "index.css",
                        to: "index.min.css"
                    },
                    {
                        from: "main.js",
                        to: "main.min.js"
                    }
                ]
            },
            imgPath: {
                src: ['dist/support/<%= pkg.name %>/css/index.css'],
                overwrite: true,                 // overwrite matched source files
                replacements: [
                    {
                        from: "../img",
                        to: "images"
                    }
                ]
            }
        },
        uncss: {
            dist: {
                files: {
                    'dist/css/tidy.css': ['src/html/index.html']
                },
                options: {
                    report: 'min' // optional: include to report savings
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.registerTask('debug', ['clean:init','concat:css', 'replace:imgPath', 'handlebars', 'requirejs', 'copy:toDist', 'copy:supportToMod', 'copy:toLivelink','clean:init']);
    grunt.registerTask('default', ['clean:init','concat:css', 'replace:imgPath', 'cssmin', 'handlebars', 'requirejs', 'copy:toDist', 'replace:indexHtml1', 'replace:indexHtml2','uglify', 'clean:release', 'copy:supportToMod','copy:toLivelink','clean:init']);
};
