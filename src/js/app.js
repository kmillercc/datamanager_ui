// Load our app
define([
    "router",
    "OScriptParser",
    "ContentServerAPI",
    "ErrorController",
    "TemplateUtils",
    "Tester",
    "routie"
], function (router, OScriptParser, ContentServerAPI, ErrorController, TemplateUtils, Tester, routie) {
    /*global App*/
    /*global LL_URL*/
    return {
        // make these accessible in the App global
        Router: router,
        OScriptParser: OScriptParser,
        ServerAPI: ContentServerAPI,
        ErrorController: ErrorController,
        TemplateUtils: TemplateUtils,
        browseSelectHtmlPath : 'browseSelect',
        tableDialogPath : 'tableDialog',
        browseExportPathHtmlPath : "be/browse_exportpath_select",
        formModalHtmlPath : "form_modal",
        browseLoadPathHtmlPath : "bl/browse_loadpath_select",
        Tester: Tester,
        routie: routie,
        ArrayUtils: function(arr){
            return {
                hasIt : false,
                contains : function(val, caseInsensitive){
                    _.each(arr, function(c){
                        switch (typeof val){
                            case "string":
                                var equals = caseInsensitive ? c.toUpperCase()===val.toUpperCase() : c===val;
                                if (equals) {this.hasIt = true;return}
                                break;
                            default:
                                equals = c===val;
                                if (equals) {this.hasIt = true;return;}
                        }
                    }, this);
                    return this.hasIt;
                }
            }
        },
        inheritFrom: function(o, parent, override){
            for (var key in parent) {
              if (parent.hasOwnProperty(key) && (!o.hasOwnProperty(key) || override)) {
                  o[key] = parent[key];

              }
            }
        }
    };
});