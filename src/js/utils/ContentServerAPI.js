define([],
    function () {
        /*global App*/
        /*global LL_URL*/

        var privObj = {
            mergeJsonObjects: function (obj1, obj2) {
                var obj3 = {};
                for (var attName in obj1) {
                    obj3[attName] = obj1[attName];
                }
                for (attName in obj2) {
                    obj3[attName] = obj2[attName];
                }
                return obj3;
            },
            ajaxToLL: function (method, func, dataParams, context, returnJqXhr) {
                if (!context) {
                    context = this;
                }
                var params = this.mergeJsonObjects({func: func}, dataParams);
                var dfd = $.Deferred();
                $.when($.ajax({type: method, url: LL_URL, data: params, context: context, dataType: 'json'}))
                    .done(function (retObj) {
                        dfd.resolve(privObj.parseLLResponse(retObj), context);
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (returnJqXhr) {
                            dfd.reject(jqXHR, textStatus, errorThrown);
                        }
                        else {
                            // default the error response to take into account a non response from server (including stack trace)
                            var errMsg;
                            if (jqXHR.status === 500 || jqXHR.statusText === "Internal server error") {
                                errMsg = "Server did not respond. Check to see if server is running. If so, are there any trace files in the Content Server logs?"
                            }
                            else if (errorThrown.length > 5 && errorThrown.substr(0, 1) === "{") {
                                try {
                                    var errObj = $.parseJSON(errorThrown);
                                    errMsg = errObj.errMsg || "Unknown Server Error"
                                }
                                catch (e) {
                                    errMsg = "Invalid JSON response from server: " + errorThrown;
                                }
                            }
                            else if (jqXHR.responseText.length > 5 && jqXHR.responseText.substr(0, 1) === "{") {
                                try {
                                    var errObj = $.parseJSON(jqXHR.responseText);
                                    errMsg = errObj.errMsg || "Unknown Server Error"
                                }
                                catch (e) {
                                    errMsg = "Invalid JSON response from server: " + errorThrown;
                                }
                            }
                            else if (typeof errorThrown === 'string' && errorThrown.length) {
                                errMsg = errorThrown;
                            }
                            else {
                                errMsg = textStatus;
                            }
                            dfd.reject(errMsg, context);
                        }
                    });

                return dfd.promise();
            },
            postToLL: function (func, dataParams, context) {
                return this.ajaxToLL("POST", func, dataParams, context);
            },
            getFromLL: function (func, dataParams, context) {
                return this.ajaxToLL("GET", func, dataParams, context);
            },
            getFromLLjqXHR: function (func, dataParams, context) {
                var dfd = $.Deferred();
                if (!context) {
                    context = this;
                }
                var params = this.mergeJsonObjects({func: func}, dataParams);
                $.when($.ajax({
                    type: "POST",
                    url: LL_URL,
                    data: params,
                    context: context,
                    dataType: 'json'
                }))
                    .done(function (retObj) {
                        dfd.resolve(retObj);
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        dfd.reject(jqXHR, textStatus, errorThrown);
                    });

                return dfd.promise();
            },
            parseLLResponse: function (retObj) {
                if (_.isObject(retObj)) {
                    // success response object is expected to have OK and result
                    if ((retObj.ok || retObj.OK) && retObj.hasOwnProperty("result")) {
                        return retObj.result;
                    }
                    else {
                        console.log("Invalid object response from LL: " + JSON.stringify(retObj));
                        return new Error("Invalid object response from LL: " + JSON.stringify(retObj));
                    }
                }
                else {
                    console.log("Invalid response from LL: " + retObj);
                    return new Error("Invalid response from LL: " + retObj);
                }
            }
        };

        return {
            /* ******   initialize functions */
            initializeLoad: function (action, colNames, optionStr, sourceSettings, context) {
                return privObj.postToLL("datamanager.initializeLoad", {
                    action: action,
                    colNames: App.OScriptParser.toOScript(colNames),
                    optionStr: optionStr,
                    sourceSettings: App.OScriptParser.toOScript(sourceSettings)
                }, context);
            },

            runExport: function (action, colNames, optionStr, sourceSettings, context) {
                return privObj.postToLL("datamanager.runExport", {
                    action: action,
                    colNames: App.OScriptParser.toOScript(colNames),
                    optionStr: optionStr,
                    sourceSettings: App.OScriptParser.toOScript(sourceSettings)
                }, context);
            },
            processChunk: function (action, jobId, items, context) {
                return privObj.postToLL("datamanager.processChunk", {
                    action: action,
                    items: App.OScriptParser.toOScript(items),
                    jobId: jobId
                }, context);
            },
            finalizeProcess: function (action, jobId, context) {
                return privObj.postToLL("datamanager.finalizeProcess", {action: action, jobId: jobId}, context);
            },

            // *** save scheduled job
            saveScheduledJob: function (type, subtype, optionStr, context) {
                return privObj.postToLL("datamanager.saveScheduledJob", {
                    type: type,
                    subtype: subtype,
                    optionStr: optionStr
                }, context);
            },

            runAsyncJob: function (scheduleId, context) {
                return privObj.postToLL("datamanager.runAsyncJob", {scheduleId: scheduleId}, context);
            },

            // *** save profile
            saveProfile: function (type, subType, optionStr, context) {
                return privObj.postToLL("datamanager.saveProfile", {
                    type: type,
                    optionStr: optionStr,
                    subType: subType
                }, context);
            },
            addProfile: function (type, subType, optionStr, context) {
                return privObj.postToLL("datamanager.addProfile", {
                    type: type,
                    optionStr: optionStr,
                    subType: subType
                }, context);
            },

            /* get functions needed for UI */
            checkFilePath: function (filePath, context) {
                return privObj.postToLL("datamanager.checkFilePath", {filePath: filePath}, context);
            },
            checkFolderPath: function (folderPath, context) {
                return privObj.postToLL("datamanager.checkFolderPath", {folderPath: folderPath}, context);
            },
            getCsvColumnsFromFile: function (filePath, context) {
                return privObj.postToLL("datamanager.getCsvColumnsFromFile", {filepath: filePath}, context)
            },
            checkCsvSize: function (filePath, context) {
                return privObj.postToLL("datamanager.checkCsvSize", {filepath: filePath}, context)
            },

            getExportPageConfig: function (pathModel, profileModel, objectTypeModel, formVals, profileFilter, context) {
                return privObj.postToLL("datamanager.getExportPageConfig", {
                    pathModel: App.OScriptParser.toOScript(pathModel),
                    profileModel: App.OScriptParser.toOScript(profileModel),
                    objectTypeModel: App.OScriptParser.toOScript(objectTypeModel),
                    formVals: App.OScriptParser.toOScript(formVals),
                    profileFilter: App.OScriptParser.toOScript(profileFilter)
                }, context)
            },

            getLoadPageConfig: function (profileModel, formVals, profileFilter, context) {
                return privObj.postToLL("datamanager.getLoadPageConfig", {
                    profileModel: App.OScriptParser.toOScript(profileModel),
                    formVals: App.OScriptParser.toOScript(formVals),
                    profileFilter: App.OScriptParser.toOScript(profileFilter)
                }, context)
            },

            getExportPaths: function (ExportPathModel, filter, forDataTable, context) {
                return privObj.getFromLL("datamanager.getExportPaths", {
                    filter: App.OScriptParser.toOScript(filter),
                    model: App.OScriptParser.toOScript(ExportPathModel),
                    forDataTable: forDataTable
                }, context)
            },
            getExportProfiles: function (ExportProfileModel, filter, forDataTable, context) {
                return privObj.getFromLL("datamanager.getExportProfiles", {
                    filter: App.OScriptParser.toOScript(filter),
                    forDataTable: forDataTable
                }, context)
            },
            getLoadPaths: function (LoadPathModel, filter, forDataTable, context) {
                return privObj.getFromLL("datamanager.getLoadPaths", {
                    filter: App.OScriptParser.toOScript(filter),
                    model: App.OScriptParser.toOScript(LoadPathModel),
                    forDataTable: forDataTable
                }, context)
            },
            getLoadProfiles: function (LoadProfileModel, filter, forDataTable, context) {
                return privObj.getFromLL("datamanager.getLoadProfiles", {
                    filter: App.OScriptParser.toOScript(filter),
                    forDataTable: forDataTable
                }, context)
            },

            getLoadJobs: function (LoadJobModel, forDataTable, context) {
                return privObj.getFromLL("datamanager.getLoadProfiles", {
                    filter: App.OScriptParser.toOScript(filter),
                    model: App.OScriptParser.toOScript(LoadProfileModel),
                    forDataTable: forDataTable
                }, context)
            },

            getExportJobs: function (ExportJobModel, filter, forDataTable, context) {
                return privObj.getFromLL("datamanager.getLoadProfiles", {
                    filter: App.OScriptParser.toOScript(filter),
                    model: App.OScriptParser.toOScript(LoadProfileModel),
                    forDataTable: forDataTable
                }, context)
            },

            getNodeTree: function (dataid, path, showFiles, doRecursive, context) {
                return privObj.getFromLL("datamanager.getNodeTree", {
                    dataid: dataid,
                    path: path,
                    showFiles: showFiles,
                    doRecursive: doRecursive
                }, context)
            },
            getRootNodeTree: function (showFiles, context) {
                return privObj.getFromLL("datamanager.getRootNodeTree", {
                    showFiles: showFiles
                }, context)
            },
            getGroupTree: function (groupId, path, showUsers, doRecursive, context) {
                return privObj.getFromLL("datamanager.getGroupTree", {
                    groupId: groupId,
                    path: path,
                    showUsers: showUsers,
                    doRecursive: doRecursive
                }, context)
            },
            getFileTree: function (filePath, doRecursive, showFiles, getMetadata, filterExtension, returnDirect, context) {
                return privObj.getFromLL("datamanager.getFileTree", {
                    filepath: filePath,
                    doRecursive: doRecursive,
                    showFiles: showFiles,
                    filterExtension: filterExtension,
                    getMetadata: getMetadata,
                    returnDirect: returnDirect
                }, context)
            },
            getNodeCount: function (path, dataid, context, includeRoot) {
                return privObj.getFromLL("datamanager.getNodeCount", {
                    dataid: dataid,
                    path: path,
                    includeRoot: includeRoot
                }, context)
            },
            getUserGroupCount: function (groupId, context, includeRoot) {
                return privObj.getFromLL("datamanager.getUserGroupCount", {
                    groupId: groupId,
                    includeRoot: includeRoot
                }, context)
            },
            getFileCount: function (filePath, context, includeRoot) {
                return privObj.getFromLL("datamanager.getFileCount", {
                    filepath: filePath,
                    includeRoot: includeRoot
                }, context)
            },
            checkJobStatus: function (jobId, context) {
                return privObj.getFromLL("datamanager.checkJobStatus", {jobId: jobId}, context)
            },
            getJobStatusHtml: function (scheduleId, context) {
                return privObj.getFromLL("datamanager.getJobStatusHtml", {scheduleId: scheduleId}, context);
            },
            getCategories: function (context) {
                return privObj.getFromLL("datamanager.getCategories", {}, context);
            },
            getObjectTypes: function (context) {
                return privObj.getFromLL("datamanager.getObjectTypes", {}, context);
            },
            getCsvColumnHtml: function (formVals, context) {
                return privObj.postToLL("datamanager.getCsvColumnHtml", {formVals: App.OScriptParser.toOScript(formVals)}, context);
            },
            getPathFromId: function (nodeId, context) {
                return privObj.getFromLL("datamanager.getPathFromId", {nodeId: nodeId}, context);
            },
            getIdFromPath: function (path, context) {
                return privObj.getFromLL("datamanager.getIdFromPath", {path: path}, context);
            },
            convertDropoffJob: function (id, jobType, action, context) {
                return privObj.postToLL("datamanager.convertDropoffJob", {id: id, jobType: jobType, action: action}, context);
            },
            getDataTableJqXHR: function (func, filter, model, fnCallback, fnErrorCallback, customCallback, context) {
                // params will come in like this
                //{type: "export", subType: "exportobjects"}
                App.PageController.showLoadingIndicator("Processing");
                $.when(privObj.getFromLLjqXHR(func, {
                    forDataTable: true,
                    filter: App.OScriptParser.toOScript(filter),
                    model: App.OScriptParser.toOScript(model)
                }, context))
                    .done(function (retObj) {
                        //log(retObj);
                        fnCallback(privObj.parseLLResponse(retObj));
                        if (customCallback) {
                            customCallback();
                        }
                        App.PageController.hideLoadingIndicator();
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        App.PageController.hideLoadingIndicator();
                        if (fnErrorCallback) {
                            fnErrorCallback(jqXHR, textStatus, errorThrown);
                        } else {
                            throw new Error(textStatus + ": " + errorThrown + " " + jqXHR.responseText);
                        }
                    });

            },
            getCsvTableJqXHR: function (func, colNames, filePath, fnCallback, fnErrorCallback, processDataFunc, startNo, displayLength, total, nodeObjects, sourceSettings, context) {
                // params will come in like this
                //{type: "export", subType: "exportobjects"}
                nodeObjects = nodeObjects ? nodeObjects : {};
                $.when(privObj.getFromLLjqXHR(func, {
                    forDataTable: true,
                    filePath: filePath,
                    colNames: App.OScriptParser.toOScript(colNames),
                    startNo: startNo,
                    total: total,
                    displayLength: displayLength,
                    nodeObjects: App.OScriptParser.toOScript(nodeObjects),
                    sourceSettings: App.OScriptParser.toOScript(sourceSettings)
                }, context))
                    .done(function (retObj) {
                        var result = privObj.parseLLResponse(retObj);
                        if (processDataFunc) {
                            processDataFunc(result.aaData);
                        }
                        try {
                            fnCallback(result);
                        } catch (e) {
                            // not sure what this error message means -- related to FixedColumns.  But it does not seem significant so I will not log it out
                            if (e.message !== "Cannot read property 'parentNode' of null") {
                                log(e);
                            }
                        }
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        App.PageController.hideLoadingIndicator();
                        if (fnErrorCallback) {
                            fnErrorCallback(jqXHR, textStatus, errorThrown);
                        } else {
                            throw new Error(textStatus + ": " + errorThrown + " " + jqXHR.responseText);
                        }
                    });

            },

            /* crud data table function used for manage load paths, load jobs, etx */
            crudDataTableOperation: function (method, func, data, context) {
                return privObj.ajaxToLL(method, func, ({data: App.OScriptParser.toOScript(data)}), context, true)
            },

            searchUsers: function (term, context) {
                return privObj.getFromLL("datamanager.searchUsers", {searchTerm: term}, context)
            },

            getTestLoadConfigs: function (context) {
                return privObj.getFromLL("datamanager.getTestLoadConfigs", {}, context)
            },

            getTestExportConfigs: function (context) {
                return privObj.getFromLL("datamanager.getTestExportConfigs", {}, context)
            }

        }

    });
