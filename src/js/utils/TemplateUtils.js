define(["templates","handlebars"],
    function (Templates, Handlebars) {
        return {
            /* global App */
            getTemplate: function (path) {
                var t = Templates[path];
                if (t){
                    return t;
                }else{
                    t = Templates.Templates[path];
                    if (t) {
                        return t;
                    }
                    else {
                    log("Could not find template in path '" + path + "'");
                    }
                }
            },

            registerPartial: function (name, path) {
                Handlebars.registerPartial(name, path);
            }
        };
    })
;