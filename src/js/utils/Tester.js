/**
 * Created by Kit on 4/28/2015.
 */

define(["async"],
    function (async) {
        return {
            /* global App */

            timeoutId : 0,

            testTimeout: function(numTimes) {

                var items =         [   {"errMsg":null, "id":1, "ok":true, "path":null, "skip":false},
                                        {"errMsg":null, "id":2, "ok":true, "path":null, "skip":false}
                                    ]

                for (var i = 0; i < numTimes; i++) {
                    $.when(App.ServerAPI.processChunk("update", 1097, items))
                        .done(function () {
                            log("success")
                        })
                        .fail(function (errMsg) {
                            log(errMsg)
                        });
                }
            },

            testLoads: function () {

                var tester = this;


                $.when(App.ServerAPI.getTestLoadConfigs(this))
                    .done(function (configs) {
                        async.eachSeries(configs, function (config, callback) {

                            var processController = App.ProcessController;
                            processController.resetProcess(false);
                            processController.EVAL_LIMIT = 5000;

                            var loadPath = config.loadPath;
                            var optionStr = config.optionStr;
                            var sourceSettings = config.sourceSettings;
                            var action = config.action;
                            var colNames = config.colNames;
                            var waitTime = config.waitTime;

                            log("");
                            log("Loading from " + loadPath);

                            $.when(App.ServerAPI.initializeProcess(action, colNames, optionStr, sourceSettings))
                                .done(function (result) {
                                    var jobId = result["jobId"];

                                    tester.timeoutId = setTimeout(function(){
                                        $.when(App.ServerAPI.checkJobStatus(jobId, this))
                                            .done(function (result) {
                                                if (result["completed"]) {
                                                    clearTimeout(tester.timeoutId);
                                                    var nodeIdArray = result["nodeIdArray"];
                                                    App.ProcessController.nodeObjects = result["nodeObjects"];
                                                    var itemsToProcess = result["itemsToProcess"];
                                                    var numErrors = result["numErrors"];
                                                    var lineErrorMap = result["lineErrorMap"];

                                                    if (numErrors) {
                                                        log("   Validation Errors: ");
                                                        _.forEach(lineErrorMap, function(n, key) {
                                                          log("         " + key + ":" + n);
                                                        });
                                                        callback(null);
                                                    }
                                                    else {
                                                        var progressBar = new App.PageController.ProgressBar("Load Progress", itemsToProcess);
                                                        processController.iterateNodes(action, jobId, function () {
                                                                log("   load completed");
                                                                callback(null);
                                                            }, function (err) {
                                                                log("   Fatal load Error: " + err);
                                                                callback(null);
                                                            }, function (err) {
                                                                log("   Non-Fatal load Error: " + err);
                                                                callback(null);
                                                            }, nodeIdArray, progressBar, App.ServerAPI.processChunk
                                                        )
                                                        ;
                                                    }

                                                } else {
                                                    log("Error: Validation not completed.")
                                                }
                                            })
                                            .fail(function (errMsg) {
                                                callback(errMsg);
                                            });


                                    }, waitTime);

                                })
                                .fail(function (errMsg) {
                                    callback(errMsg);
                                });


                        }, function (err) {
                            if (err) {
                                log(err);
                            }
                        });
                    })
                    .fail(function (errMsg) {
                        log(errMsg);
                    });


            },

            testExports: function () {

                $.when(App.ServerAPI.getTestExportConfigs(this))
                    .done(function (configs) {
                        async.eachSeries(configs, function (config, callback) {

                            var optionStr = config.optionStr;
                            var colNames = config.colNames;
                            var action = config.action;
                            var testName = config.testName;
                            var sourceSettings = config.sourceSettings;

                            $.when(App.ServerAPI.initializeProcess(action, colNames, optionStr, sourceSettings))
                                .done(function (result) {
                                    var jobId = result["jobId"];
                                    $.when(App.ServerAPI.checkJobStatus(jobId, this))
                                        .done(function (result) {
                                            if (result["completed"]) {
                                                var numErrors = result["numErrors"];
                                                var lineErrorMap = result["lineErrorMap"];

                                                if (numErrors) {
                                                    log(testName + "-- Export Errors: ");
                                                    log(lineErrorMap);
                                                    callback(null);
                                                }
                                                else {
                                                    log(testName + "-- Export Successful");
                                                    callback(null);

                                                }

                                            } else {
                                                callback("error: export not completed")
                                            }
                                        })
                                        .fail(function (errMsg) {
                                            callback(errMsg);
                                        });

                                })
                                .fail(function (errMsg) {
                                    callback(errMsg);
                                });


                        }, function (err) {
                            if (err) {
                                log(err);
                            }
                        });
                    })
                    .fail(function (errMsg) {
                        log(errMsg);
                    });

            }
        };
    })
;
