define(["CsvPageController", "DataTableCsvController", "LoadValidateController", "ProfileController", "JobScheduleController", "TreeController"],
    function (CsvPageController, DataTableCsvController, LoadValidateController, ProfileController, JobScheduleController, TreeController) {
        /*global App*/
        /*global log*/
        return function () {

            App.SourceViewController = App.DataTableController = new DataTableCsvController({columnHighlighting: true});
            App.ProcessController = new LoadValidateController();
            App.ProfileController = ProfileController;
            App.ProfileController.initPage();
            App.JobScheduleController = new JobScheduleController();
            App.JobScheduleController.initPage();
            App.TargetViewController = new TreeController("targetDiv");

            var controller = Object.create(CsvPageController);

            controller.initPage = function (formVals, callback) {
                var controller = this;
                CsvPageController.initPage.call(this, formVals, callback);

                // setup all event listeners specific to this page
                $("#selectTargetPathButton").click(function (evt) {
                    controller.openModalWindow('<div id="myModal"><img class="progress-img"></div>', null, true, "modal_progress_indicator_div", false);
                    App.TargetPathSelectController = new TreeController("treeDiv_select", "myModal");
                    var html = App.TemplateUtils.getTemplate(App.browseSelectHtmlPath)({});
                    App.TargetPathSelectController.loadLLTreeInModal(ENT_WSPACE_ID, ENT_WSPACE_NAME, html, "Select Target Folder", false, controller.targetNodeModalActivate, controller.targetPathSelectClicked);
                });

                $("#actionMetadata").click(function (e) {
                    controller.updatePage();
                });

                var addVersionIfNewerCbx = $("#docOptionAddVersionIfNewer");
                var addVersionCbx = $("#docOptionAddVersion");

                addVersionCbx.click(function (e) {
                    if (addVersionCbx.prop("checked")) {
                        addVersionIfNewerCbx.prop('checked', false);
                    }
                });

                addVersionIfNewerCbx.click(function (e) {
                    if (addVersionIfNewerCbx.prop("checked")) {
                        addVersionCbx.prop('checked', false);
                    }
                });

                $("input[name='containerOption']").click(function (e) {
                    controller.updatePage();
                });

                $("input[name='docOption']").click(function (e) {
                    controller.updatePage();
                });

                $("#autoCreateFolders").click(function (e) {
                    controller.updatePage();
                });


                $("#rootFileLoc").change(function (e) {
                    controller.rootFileLoc = $("#rootFileLoc").val();
                    controller.paramsChanged();
                });

            };

            controller.initFinalDisplayAdjust = function (formVals) {
                // do all the accordion elements
                $("#metadataOptionsDiv").accordion({
                    active: false,
                    collapsible: true,
                    heightStyle: 'content'
                });
                $("#actionOptionsDiv").accordion({
                    active: false,
                    collapsible: true,
                    heightStyle: 'content'
                });
                $("#copyOptionsDiv").accordion({
                    active: 0,
                    collapsible: true,
                    heightStyle: 'content'
                });

                if (typeof formVals !== "undefined" && formVals["rootSourcePath"]) {
                    var val = formVals["rootSourcePath"];
                    var when = $("input:radio[name=whenOption]:checked").val();
                    var doOpenCsv = when === "now" || controller.isCsvPath(val);
                    var setAction = typeof formVals["action"] === "undefined";
                    controller.setCsvPathFromRouter(val, doOpenCsv, setAction);
                }

                // default in a target of enterprise if it wasn't already brought into the form
                if (typeof formVals === "undefined" || !formVals["rootTargetPath"]) {
                    controller.setTargetPath(ENT_WSPACE_ID, ENT_WSPACE_NAME, false);
                }

                if (typeof formVals === "undefined" || !formVals["action"]) {
                    // initialize action to "create" (first item in the pulldown)
                    controller.setAction('create', false);
                }

                controller.showElement("containerOptionDiv");

                // finally update the page to ensure everything is set correctly
                controller.updatePage();
            };

            controller.setAction = function (action, updatePage) {
                $("#action").val(action);
                controller.actionToPerform = action;

                // email fields can only be set at create time
                if (action === "create") {
                    controller.showElement("mdEmailLabel");
                }
                else {
                    controller.hideElement("mdEmailLabel");
                }

                if (action === "move") {
                    $("#actionsHeading").html("Post-Move Actions");
                    $("#metadataHeading").html("Metadata to Update");
                }
                else if (action === "copy") {
                    $("#actionsHeading").html("Post-Copy Actions (on the new objects)");
                    $("#metadataHeading").html("Metadata to Update");
                }
                else if (action === "create") {
                    $("#actionsHeading").html("Post-Create Actions");
                    $("#metadataHeading").html("Metadata to Set");
                }
                else if (action === "update") {
                    $("#actionsHeading").html("Update Actions");
                    $("#metadataHeading").html("Metadata to Update");
                }
                else if (action === "addversion") {
                    $("#actionsHeading").html("Post-Add Version Actions");
                    $("#metadataHeading").html("Metadata to Update");
                }

                controller.hideShowNameConflictDiv(action);
                controller.hideShowAdvancedOptions(action);
                controller.hideShowAddVersionOptions(action);
                controller.hideShowFileLocOptions(action);
                controller.hideShowMetadataOptions(action);
                controller.hideShowActionOptions(action);

                if (updatePage) {
                    controller.enableDisableFromDataFlags(controller.dataFlags);
                    controller.updatePage();
                }
            };

            controller.targetPathSelectClicked = function () {
                controller.closeModalWindow();
                controller.setTargetPath(controller.modalTargetIdSelected, controller.modalTargetPathSelected, true);
            };

            controller.setTargetPath = function (dataId, path, doUpdate) {
                $('#rootTargetPath').val(path);
                controller.rootTargetId = dataId;
                controller.rootTargetPath = path;
                if (doUpdate) {
                    controller.paramsChanged();
                    controller.updatePage();
                }
            };

            controller.targetNodeModalActivate = function (node) {
                if (node.data.unselectable) {
                    controller.disableElement("selectButton");
                    controller.modalTargetIdSelected = 0;
                    controller.modalTargetPathSelected = '';
                } else {
                    controller.enableElement("selectButton");
                    controller.modalTargetIdSelected = node.data.dataid;
                    controller.modalTargetPathSelected = node.data.path;
                }
            };

            controller.hideShowFileLocOptions = function (action) {
                if (action === "addversion" || action === "create") {
                    controller.showElement("rootFileLocControl");
                }
                else {
                    controller.hideElement("rootFileLocControl");
                }
            };

            controller.hideShowAddVersionOptions = function (action) {
                if (action === "addversion") {
                    controller.showElement("addVersionControlGroup");
                }
                else {
                    controller.hideElement("addVersionControlGroup");
                }
            };
            controller.hideShowNameConflictDiv = function (action) {
                //if (action == "create" || action == "move" || action == "copy") {
                // *** TODO - support name collisions with MOVE AND COPY
                if (action === "create") {
                    controller.showElement("nameCollisionFieldset");
                } else {
                    controller.hideElement("nameCollisionFieldset");
                }
            };

            controller.setActionFromCsv = function (colNames) {
                var currentAction = App.PageController.actionToPerform;

                if (App.ArrayUtils(colNames).contains("$Action", true)) {
                      if (currentAction !== "update") {
                        App.PageController.setAction("dynamic", true);
                        controller.showInfo("Please note: the operation has been set to 'Dynamic' based on the presence of the '$Action' column in the CSV.", true);
                      }
                } else if (App.ArrayUtils(colNames).contains("$ObjectId", true)) {
                    if (currentAction !== "update") {
                        App.PageController.setAction("update", true);
                        controller.showInfo("Please note: the operation has been set to 'Update' based on the presence of the '$ObjectId' column in the CSV.", true);
                    }
                } else {
                    if (currentAction !== "create") {
                        App.PageController.setAction("create", true);
                        controller.showInfo("Please note: the operation has been set to 'Create' based on the absence of the '$ObjectId' column in the CSV.", true);
                    }
                }

            };

            controller.hideShowActionOptions = function (action) {

                if (action === "delete" || action === "purge") {
                    controller.hideElement("actionOptionsControlGroup");

                    $('#actionRename').prop('checked', false);
                    $("#actionMetadata").prop("checked", false);

                    $("#actionPoCirc").prop("checked", false);
                    $("#actionPoBox").prop("checked", false);
                    $("#actionPoLocator").prop("checked", false);
                    $("#actionPoXFer").prop("checked", false);
                    $("#actionPoLabel").prop("checked", false);

                    $("#actionPerms").prop("checked", false);

                } else {

                    controller.showElement("actionOptionsControlGroup");

                    // show these initially
                    controller.showElement("actionRenameLabel");
                    controller.showElement("actionRenameLabel");
                    controller.showElement("actionMetadataLabel");
                    controller.showElement("actionPermsLabel");

                    // hide these initially
                    controller.hideElement("actionPoCircLabel");
                    $('#actionPoCirc').prop('checked', false);
                    controller.hideElement("actionPoBoxLabel");
                    $('#actionPoBox').prop('checked', false);
                    controller.hideElement("actionPoLocatorLabel");
                    $('#actionPoLocator').prop('checked', false);
                    controller.hideElement("actionPoXFerLabel");
                    $('#actionPoXFer').prop('checked', false);
                    controller.hideElement("actionPoLabelLabel");
                    $('#actionPoLabel').prop('checked', false);


                    if (action === "create") {

                        controller.hideElement("actionRenameLabel");
                        $('#actionRename').prop('checked', false);
                        controller.hideElement("actionMetadataLabel");
                        $('#actionMetadata').prop('checked', false);
                    }

                    if (action === "move" || action === "copy") {
                        controller.hideElement("actionRenameLabel");
                        $('#actionRename').prop('checked', false);
                    }

                    if (action === "copy") {
                        controller.showElement("copyOptionsControlGroup");
                    } else {
                        controller.hideElement("copyOptionsControlGroup");
                    }

                    if (action === 'update') {
                        controller.showElement("actionPoCircLabel");
                        controller.showElement("actionPoBoxLabel");
                        controller.showElement("actionPoLocatorLabel");
                        controller.showElement("actionPoXFerLabel");
                        controller.showElement("actionPoLabelLabel");
                    }
                }
            };

            controller.hideShowMetadataOptions = function (action) {
                if (action === "delete" || action === "purge") {
                    controller.hideElement("metadataOptionsControlGroup");
                    $('#mdCats').prop('checked', false);
                    $('#mdClass').prop('checked', false);
                    $('#mdDapi').prop('checked', false);
                    $('#mdSysAttrs').prop('checked', false);
                    $('#mdNickname').prop('checked', false);
                    $('#mdOwner').prop('checked', false);
                    $('#mdPo').prop('checked', false);
                    $('#mdRm').prop('checked', false);
                    $('#mdEmail').prop('checked', false);
                }
            };

            controller.enableDisableFromDataFlags = function (dataFlags) {
                var action = controller.actionToPerform;
                var isScheduled = controller.isScheduled();
                $("#actionOptionsDiv").accordion("option", "active", 0);

                // handle enabling/disabling actions
                if (dataFlags["hasNewName"] || isScheduled) {
                    controller.enableElement('actionRename');
                    $(".actionRenameLabel").css("color", "");
                    $('#actionRename').prop('checked', !isScheduled);
                } else {
                    $(".actionRenameLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionRename');
                    $('#actionRename').prop('checked', false);
                }

                if ((dataFlags["hasMetadataCols"] && action !== "create" && action !== "purge" && action !== "delete") || isScheduled) {
                    controller.enableElement('actionMetadata');
                    $("#actionMetadataLabel").css("color", "");
                    $('#actionMetadata').prop('checked', !isScheduled);
                    $("#metadataOptionsDiv").accordion("option", "active", 0);
                } else {
                    $("#actionMetadataLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionMetadata');
                    $('#actionMetadata').prop('checked', false);
                }

                if (dataFlags["hasPermCols"] || isScheduled) {
                    controller.enableElement('actionPerms');
                    $("#actionPermsLabel").css("color", "");
                    $('#actionPerms').prop('checked', !isScheduled);
                } else {
                    $("#actionPermsLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPerms');
                    $('#actionPerms').prop('checked', false);
                }

                if (dataFlags["hasPoCircCols"] || isScheduled) {
                    controller.enableElement('actionPoCirc');
                    $("#actionPoCircLabel").css("color", "");
                    $('#actionPoCirc').prop('checked', !isScheduled);
                } else {
                    $("#actionPoCircLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPoCirc');
                    $('#actionPoCirc').prop('checked', false);
                }

                if (dataFlags["hasPoBoxCols"] || isScheduled) {
                    controller.enableElement('actionPoBox');
                    $("#actionPoBoxLabel").css("color", "");
                    $('#actionPoBox').prop('checked', !isScheduled);
                } else {
                    $("#actionPoBoxLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPoBox');
                    $('#actionPoBox').prop('checked', false);
                }

                if (dataFlags["hasPoLocatorCols"] || isScheduled) {
                    controller.enableElement('actionPoLocator');
                    $("#actionPoLocatorLabel").css("color", "");
                    $('#actionPoLocator').prop('checked', !isScheduled);
                } else {
                    $("#actionPoLocatorLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPoLocator');
                    $('#actionPoLocator').prop('checked', false);
                }

                if (dataFlags["hasPoXferCols"] || isScheduled) {
                    controller.enableElement('actionPoXFer');
                    $("#actionPoXFerLabel").css("color", "");
                    $('#actionPoXFer').prop('checked', !isScheduled);
                } else {
                    $("#actionPoXFerLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPoXFer');
                    $('#actionPoXFer').prop('checked', false);
                }

                if (dataFlags["hasPoLabelCols"] || isScheduled) {
                    controller.enableElement('actionPoLabel');
                    $("#actionPoLabelLabel").css("color", "");
                    $('#actionPoLabel').prop('checked', !isScheduled);
                } else {
                    $("#actionPoLabelLabel").css("color", "#d3d3d3");
                    controller.disableElement('actionPoLabel');
                    $('#actionPoLabel').prop('checked', false);
                }

                // handle enabling disabling metadata options
                if ((dataFlags["hasEmailCols"] || isScheduled) && action === "create") {
                    controller.enableElement('mdEmail');
                    $('#mdEmail').prop('checked', true);
                    $("#mdEmailLabel").css("color", "");
                } else {
                    controller.disableElement('mdEmail');
                    $("#mdEmailLabel").css("color", "#d3d3d3");
                    $('#mdEmail').prop('checked', false);
                }
                if (dataFlags["hasAttrCols"] || isScheduled) {
                    controller.enableElement('mdCats');
                    $('#mdCats').prop('checked', true);
                    $("#mdCatsLabel").css("color", "");
                } else {
                    controller.disableElement('mdCats');
                    $("#mdCatsLabel").css("color", "#d3d3d3");
                    $('#mdCats').prop('checked', false);
                }
                if (dataFlags["hasClassCols"] || isScheduled) {
                    controller.enableElement('mdClass');
                    $("#mdClassLabel").css("color", "");
                    $('#mdClass').prop('checked', true);
                } else {
                    controller.disableElement('mdClass');
                    $("#mdClassLabel").css("color", "#d3d3d3");
                    $('#mdClass').prop('checked', false);
                }
                if (dataFlags["hasDapiNodeCols"] || isScheduled) {
                    controller.enableElement('mdDapi');
                    $("#mdDapiLabel").css("color", "");
                    $("#mdDapiHelp").css("color", "");
                    $('#mdDapi').prop('checked', true);
                } else {
                    controller.disableElement('mdDapi');
                    $("#mdDapiLabel").css("color", "#d3d3d3");
                    $("#mdDapiHelp").css("color", "#d3d3d3");
                    $('#mdDapi').prop('checked', false);
                }
                if (dataFlags["hasSystemAttrCols"] || isScheduled) {
                    controller.enableElement('mdSysAttrs');
                    $("#mdSysAttrsLabel").css("color", "");
                    $("#mdSysAttrsHelp").css("color", "");
                    $('#mdSysAttrs').prop('checked', true);
                } else {
                    controller.disableElement('mdSysAttrs');
                    $("#mdSysAttrsLabel").css("color", "#d3d3d3");
                    $("#mdSysAttrsHelp").css("color", "#d3d3d3");
                    $('#mdSysAttrs').prop('checked', false);
                }
                if (dataFlags["hasNickName"] || isScheduled) {
                    controller.enableElement('mdNickname');
                    $("#mdNicknameLabel").css("color", "");
                    $('#mdNickname').prop('checked', true);
                } else {
                    $("#mdNicknameLabel").css("color", "#d3d3d3");
                    controller.disableElement('mdNickname');
                    $('#mdNickname').prop('checked', false);
                }
                if (dataFlags["hasOwnerCols"] || isScheduled) {
                    $("#mdOwnerLabel").css("color", "");
                    controller.enableElement('mdOwner');
                    $('#mdOwner').prop('checked', true);
                } else {
                    $("#mdOwnerLabel").css("color", "#d3d3d3");
                    controller.disableElement('mdOwner');
                    $('#mdOwner').prop('checked', false);
                }
                if (dataFlags["hasPoCols"] || isScheduled) {
                    $("#mdPoLabel").css("color", "");
                    controller.enableElement('mdPo');
                    $('#mdPo').prop('checked', true);
                } else {
                    $("#mdPoLabel").css("color", "#d3d3d3");
                    controller.disableElement('mdPo');
                    $('#mdPo').prop('checked', false);
                }
                if (dataFlags["hasRmCols"] || isScheduled) {
                    $("#mdRmLabel").css("color", "");
                    controller.enableElement('mdRm');
                    $('#mdRm').prop('checked', true);
                } else {
                    $("#mdRmLabel").css("color", "#d3d3d3");
                    controller.disableElement('mdRm');
                    $('#mdRm').prop('checked', false);
                }
            };


            controller.updatePage = function () {
                CsvPageController.updatePage.call(this);
                var action = controller.actionToPerform;

                if (!CS_HAS_EMAIL_MODULE) {
                    $(".otemail").hide();
                }
                if (!CS_HAS_PO_MODULE) {
                    $(".otpo").hide();
                }
                if (!CS_HAS_RM_MODULE) {
                    $(".otrecman").hide();
                }

                // root path control?
                if (action === "update") {
                    controller.hideElement("rootFileLocControl");
                }
                else {
                    controller.showElement("rootFileLocControl");
                }
                var contUpOption = $("#containerOptionUpdate");
                var contUpMdOption = $("#containerOptionUpdateMd");
                var docOptionUpdate = $("#docOptionUpdate");

                if ($("#autoCreateFolders").prop("checked")) {
                    $("input[name='containerOption']").prop('disabled', true);
                    contUpOption.prop("disabled", false);
                    contUpOption.prop('checked', true);
                }
                else {
                    $("input[name='containerOption']").prop('disabled', false);
                }

                if (docOptionUpdate.prop("checked")) {
                    $(".docUpdateLabel").css("color", "");
                    $("#docOptionUpdateMd").prop("checked", true);
                } else {
                    $(".docUpdateLabel").css("color", "#d3d3d3");
                    $(".docUpdateCheckbox").prop('checked', false);
                }

                if (contUpOption.prop("checked")) {
                    contUpMdOption.prop('checked', true);
                    contUpMdOption.on("click", function (e) {
                        e.preventDefault();
                    });
                    $(".containerUpdateLabel").css("color", "");
                } else {
                    $(".containerUpdateLabel").css("color", "#d3d3d3");
                    $(".containerUpdateCheckbox").prop('checked', false);
                    contUpMdOption.on("click", function (e) {
                        // do nothing
                    });
                }

                if ($("#actionMetadata").prop("checked") || action === "create") {
                    controller.showElement("metadataOptionsControlGroup");
                } else {
                    controller.hideElement("metadataOptionsControlGroup");
                }

            };

            return controller;
        }
    }
)
;
