define(["RootPageController", "LoadJobModel", "DataTableCrudController"],
    function (RootPageController, LoadJobModel, DataTableCrudController) {
        return function () {
            /*global App*/
            /*global LL_URL*/

            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Load Job");
                App.DataTableController.setApiFuncs("datamanager.getLoadJobs", "datamanager.addEditLoadJob", "datamanager.addEditLoadJob", "datamanager.removeLoadJob");

                // *** on switch page, make sure we clear out any real time updating from jobs page
                _.each(App.intervalIds, function (intervalId) {
                    window.clearInterval(intervalId);
                    App.log("cleared intervalId " + intervalId)
                });
                App.intervalIds = [];

                    // callback
                    if (callback){
                        callback();
                    }

                App.DataTableController.initController(LoadJobModel, false, {jobtype: 'load'}, false, 'scheduleId', controller.dataTableCallback)
            };


            return controller;
        };
    });