define(["ProcessController"],
    function (ProcessController) {
        /*global App*/
        return function () {
            var controller = Object.create(ProcessController);

            controller.logLoadTimings = function () {
                var rate = (this.stopWatch.getDurationSeconds() / this.itemsToProcess).toFixed(3);
                log("Load Timings:");
                log("   " + this.itemsToProcess + " items processed");
                log("   " + this.stopWatch.getDurationStr() + " elapsed time");
                log("   " + rate + " seconds per item");
                this.stopWatch.stop();
            };

            controller.logValidateTimings = function () {
                var rate = (this.stopWatch.getDurationSeconds() / this.itemsInitialized).toFixed(3);
                log("Validate Timings:");
                log("   " + this.itemsInitialized + " items initialized");
                log("   " + this.stopWatch.getDurationStr() + " elapsed time");
                log("   " + rate + " seconds per item");
                this.stopWatch.stop();
            };

            controller.runValidation = function (optionStr) {

                // start the stopWatch
                this.stopWatch.start();

                var context = this;

                $.when(controller.initializeLoad(optionStr, controller))
                    .done(function (result) {
                        controller.showInitializeProgress(controller.jobId, controller.fatalValidationError, controller.nonFatalValidationError, controller.validateSuccess, "Validating", context, true, "Valid");
                    })
                    .fail(function (errMsg) {
                        App.PageController.showError(errMsg);
                    });
            };

            controller.runCsvGeneration = function (optionStr) {

              // start the stopWatch
              this.stopWatch.start();

              var context = this;

              $.when(controller.initializeCsvGeneration(optionStr, controller))
                .done(function (result) {
                  controller.showInitializeProgress(controller.jobId, controller.fatalValidationError, controller.nonFatalValidationError, controller.validateSuccess, "Generating", context, true, "Valid");
                })
                .fail(function (errMsg) {
                  App.PageController.showError(errMsg);
                });
            };

            controller.runLoad = function () {

                var action = $("#action").val();

                if ((action === "create" || action === "addversion") && $('#removeImportedFiles').prop( "checked" )){
                    if (App.PageController.confirm("Delete source files in your Load Directory? You won't be able to undo this operation.",
                        function(result){
                            if (result){
                                // start the stopWatch
                                controller.stopWatch.start();
                                controller.callLoadIteration();
                            }
                        }
                        ));
                }
                else{
                    // start the stopWatch
                    this.stopWatch.start();
                    controller.callLoadIteration();
                }
            };

            /*
            controller.callValidateIteration = function () {

                controller.logValidateTimings();

                var progressBar = new App.PageController.ProgressBar("Validation Progress", controller.itemsToProcess);
                controller.iterateNodes(controller.jobId, controller.validateSuccess, controller.fatalValidationError, controller.nonFatalValidationError, controller.nodeIdArray, progressBar, App.ServerAPI.validateChunk);
            };
            */

            controller.callLoadIteration = function () {

                var progressBar = new App.PageController.ProgressBar("Load Progress", controller.itemsToProcess);
                controller.iterateNodes(App.PageController.actionToPerform, controller.jobId, controller.finalizeLoad, controller.fatalLoadError, controller.nonFatalLoadError, controller.nodeIdArray, progressBar, App.ServerAPI.processChunk);
            };

            controller.finalizeLoad = function (showSuccess) {
                var dfd = $.Deferred();

                controller.logLoadTimings();

                showSuccess = (typeof showSuccess == "undefined") ? true : showSuccess;
                App.PageController.showLoadingIndicator("Logging Results");

                $.when(App.ServerAPI.finalizeProcess(App.PageController.actionToPerform, App.ProcessController.jobId, this))
                    .done(function (result) {
                        App.PageController.hideLoadingIndicator();
                        if (showSuccess) {
                            App.ProcessController.loadSuccess(result);
                        }
                        dfd.resolve(result);
                    })
                    .fail(function (errMsg) {
                        App.PageController.hideLoadingIndicator();
                        App.PageController.showError(errMsg);
                        dfd.reject(errMsg)
                    });

                return dfd.promise()
            };

            controller.validateSuccess = function () {

                controller.logValidateTimings();
                App.PageController.showSuccess("Validation completed with no errors.");

                //while(App.PageController)
                App.PageController.setValidForLoad();

            };

            controller.nonFatalValidationError = function (result) {
                var msg = result["msg"] || "There were " + result.nonFatalErrors + " errors. The errors are shown in CSV status column and have been logged to the '_audit' folder in the load directory. You must fix these errors before you can load.";
                App.PageController.showError(msg);
            };

            controller.nonFatalLoadError = function (result) {
                var msg = result["msg"] || "The load completed, but there were " + result.nonFatalErrors + " errors.  Errors have been logged to the '_audit' folder in the load directory.";

                $.when(controller.finalizeLoad(true))
                    .done(function () {
                        App.PageController.showWarning(msg);
                    })
                    .fail(function (errMsg, context) {
                        // do nothing
                    })
            };

            controller.loadSuccess = function (result) {
                var msg = result["msg"] || "Load completed successfully. Results have been logged to the '_audit' folder in the load directory."
                App.PageController.showSuccess(msg);
                if (App.PageController.setTargetPath) {
                    App.PageController.setTargetPath(App.PageController.rootTargetId, App.PageController.rootTargetPath);
                }
            };

            controller.setInvalidForLoad = function () {
                // just set the jobId to 0
                controller.jobId = 0;
                App.PageController.setInvalidForLoad();
            };

            controller.fatalValidationError = function (err) {
                controller.logLoadTimings();
                controller.setInvalidForLoad();
                App.PageController.showError(err);
            };

            controller.fatalLoadError = function (err) {
                controller.logLoadTimings();
                App.PageController.showError(err);
                controller.setInvalidForLoad();
            };

            return controller;

        }
    });
