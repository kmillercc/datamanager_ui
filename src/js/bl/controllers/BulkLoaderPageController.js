define(["RootPageController", "LoadPathModel", "LoadProfileModel", "TreeController"],
    function (RootPageController, LoadPathModel, LoadProfileModel, TreeController) {
        /*global App*/
        var controller = Object.create(RootPageController);
        controller.filePath = '';
        controller.modalSourcePathSelected = null;
        controller.modalTargetPathSelected = null;
        controller.modalTargetIdSelected = null;
        controller.actionToPerform = "create";
        controller.rootSourcePath = null;
        controller.rootTargetPath = null;
        controller.rootTargetId = null;
        controller.isValidForLoad = false;

        controller.getHiddenCols = function (rawColNames) {
            return []
        };

        controller.initPage = function (formVals, callback) {
            RootPageController.initPage.call(this, formVals);

            controller.setInvalidForLoad();

            controller.profileModel = LoadProfileModel;

          // *** make 1 call to the server to get everything we need for this page
            $.when(App.ServerAPI.getLoadPageConfig(LoadProfileModel, formVals, {jobSubtype: $('#jobSubtype').val()}))
                .done(function (data) {

                    // handle profiles
                    var profiles = data["profiles"];
                    controller.setSelectOptions("profileId", "profileId", "profileId", profiles, false,
                        function () {
                            $("#profileId").combobox();
                            $("#toggle").click(function () {
                                log('clicked');
                                $("#profileId").toggle()
                            });
                        });

                    // callback
                    if (callback){
                        callback();
                    }

                })
                .fail(function (errMsg) {
                    App.PageController.showError(errMsg);
                });


            // generic callback to set paramsChanged for all radio and checkbox controls
            $("input[type='radio'], input[type='checkbox']").change(function (e) {
                controller.paramsChanged();
            });

            $("#validateButton").click(function (e) {
                e.preventDefault();
                controller.validateButtonClicked();
            });

            $("#loadButton").click(function (e) {
                e.preventDefault();
                controller.loadButtonClicked();

            });

            $("#useFileCreateDate").click(function (e) {
                if ($("#useFileCreateDate").prop('checked')) {
                    $("#useFileModifyDate").prop('checked', false);
                }
            });

            $("#useFileModifyDate").click(function (e) {
                if ($("#useFileModifyDate").prop('checked')) {
                    $("#useFileCreateDate").prop('checked', false);
                }
            });

            $("#selectSourcePathButton").click(function (evt) {
                App.SourcePathSelectController = new TreeController("treeDiv_select", "myModal");
                var html = App.TemplateUtils.getTemplate(App.browseLoadPathHtmlPath)({help_block: App.PageController.helpText});
                controller.openModalWindow(html, App.PageController.sourceModalTital, true, null, null, "auto", "500px");
                $("#selectButton").click(function (e) {
                    e.preventDefault();
                    App.PageController.sourcePathSelectClicked();
                });
                controller.getSetSelectOptions(LoadPathModel, "path", "path", "startPath", App.ServerAPI.getLoadPaths, {}, true, null);

                $("#startPath").change(function (evt) {
                    controller.paramsChanged();
                    var path = $("#startPath").val();
                    if (path.length) {
                        controller.folderPath = path;
                        var filterExtension = App.PageController.pageType === "csv" ? "csv" : null;
                        App.SourcePathSelectController.loadFileTreeSelect(path, App.PageController.showSourceFilesSelect, App.PageController.sourceNodeModalActivate, filterExtension);
                    }
                });
            });

            var input = $("#fileRemovalPath");
            var inputVal = input.val();
            if (!inputVal || inputVal===""){
                // put file removal path in the text box
                input.val(FILE_REMOVAL_PATH);
            }

        };

        controller.validateButtonClicked = function () {
            App.ProcessController.runValidation(App.PageController.serializeFormVals());
        };

        controller.loadButtonClicked = function () {
            App.ProcessController.runLoad();
        };

        controller.hideShowAdvancedOptions = function (action) {
            // *** initially hide all of the items
            $('div.advanced_settings > label').hide();

            // now show just the ones for this action
            $('div.advanced_settings > label.bl_' + action).show();
        };

        controller.paramsChanged = function () {
            App.ProcessController.setInvalidForLoad();
            controller.setInvalidForLoad();

        };

        controller.updatePage = function () {
            RootPageController.updatePage.call(this);
        };

        controller.setInvalidForLoad = function () {
            controller.isValidForLoad = false;
            controller.disableActionButtons(false);
        };

        controller.setValidForLoad = function () {
            controller.isValidForLoad = true;
            controller.enableActionButtons();
        };

        controller.setRootTargetPath = function(path){

            $.when(App.ServerAPI.getIdFromPath(path))
                .done(function (dataId) {
                    App.PageController.setTargetPath(dataId, path, true);
                })
                .fail(function (errMsg) {
                    App.PageController.showError(errMsg);
                });

        };

        controller.enableActionButtons = function () {
            if (this.isNow()) {
                if (App.PageController.isValidForLoad) {
                    this.enableElement("loadButton");
                    this.showElement("loadButton");
                }
                else {
                    this.enableElement("validateButton");
                }
            } else{
                this.enableElement("validateButton");
            }
        };

        controller.disableActionButtons = function (disableValidate) {
            if (this.isNow()){
                if (disableValidate){
                    this.disableElement("validateButton");
                }
                this.disableElement("loadButton");
                this.hideElement("loadButton");
            } else{
                this.enableElement("validateButton");
            }
        };

        return controller;
    });
