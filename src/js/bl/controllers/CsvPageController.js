define(["BulkLoaderPageController"],
    function (BulkLoaderPageController) {
        /*global App*/
        /*global log*/
        var controller = Object.create(BulkLoaderPageController);
        controller.loadfrom = "server";
        controller.hasCsvOpen = false;
        controller.rootFileLoc = "absolute";
        controller.modalSourcePathSelected = null;
        controller.pageType = "csv";
        controller.sourceModalTital = "Select a CSV File";
        controller.helpText = "Select a CSV file and then click 'Select'";
        controller.showSourceFilesSelect = true;
        controller.allowFolders = false;
        controller.colNames = [];
        controller.dataFlags = {};

        controller.initPage = function (formVals, callback) {
            BulkLoaderPageController.initPage.call(this, formVals, callback);
            controller.hideGrid();
            App.DataTableController.initController();

            $("#action").change(function (e) {
                e.preventDefault();
                var action = $("#action").val();
                App.PageController.paramsChanged();
                App.PageController.setAction(action, true);
            });
        };

        controller.enableDisableFromDataFlags = function () {
            // implemented in subclasses
        };


        controller.setActionFromCsv = function (colNames) {
            // implemented in subclasses
        };

        controller.openServerCsv = function (filePath, setActionFromCsv, displayLen, setFieldsFromCsv) {
            displayLen = displayLen || 10;
            var controller = this;

            this.showLoadingIndicator("Reading File");
            // *** check to make sure we are not over the limit
            $.when(App.ServerAPI.checkCsvSize(filePath))
                .done(function (numRows) {
                    controller.setEstimatedTotal(numRows);
                    if (controller.checkTotalNumItems(numRows)) {
                        $.when(App.ServerAPI.getCsvColumnsFromFile(filePath))
                            .done(function (result) {
                                App.PageController.colNames = result.colNames;
                                var dataFlags = result["dataFlags"];
                                App.PageController.dataFlags = dataFlags;
                                var colNames = result["colNames"];
                                App.DataTableController.doServerCsv(numRows, filePath, result.colNames, function (result) {
                                    controller.gridLoadDone();
                                    if (setFieldsFromCsv) {
                                      // adjust action options
                                      App.PageController.enableDisableFromDataFlags(dataFlags);
                                      App.PageController.updatePage();
                                    }
                                    if (setActionFromCsv) {
                                        App.PageController.setActionFromCsv(colNames);
                                    }
                                }, function (jqXHR, textStatus, errorThrown) {
                                    controller.hideLoadingIndicator();
                                    controller.hasCsvOpen = false;
                                    controller.showError(errorThrown);
                                    App.PageController.updatePage();
                                }, displayLen);
                            })
                            .fail(function (errMsg) {
                                controller.hideLoadingIndicator();
                                controller.showError(errMsg);
                            });

                    }
                })
                .fail(function (errMsg) {
                    controller.showError(errMsg);
                });

        };

        controller.gridLoadDone = function () {
            this.hideLoadingIndicator();
            BulkLoaderPageController.gridLoadDone.call(this);
            this.hasCsvOpen = true;
            App.PageController.updatePage();
            App.ProcessController.resetProcess(true);
        };

        controller.reloadGrid = function (displayLen) {
            displayLen = displayLen || 10;
            App.DataTableController.reset();
            this.openServerCsv($('#rootSourcePath').val(), false, displayLen, true);
        };

        controller.setCsvPath = function (path, doOpenCsv, autoSelectAction) {
            autoSelectAction = autoSelectAction || false;
            $('#rootSourcePath').val(path);
            if (doOpenCsv) {
                controller.openServerCsv(path, autoSelectAction, 10, true);
            }
        };

      controller.setCsvPathFromRouter = function (path, doOpenCsv, autoSelectAction) {
        autoSelectAction = autoSelectAction || false;
        $('#rootSourcePath').val(path);
        if (doOpenCsv) {
          controller.openServerCsv(path, autoSelectAction, 10, false);
        }
      };

        controller.sourceNodeModalActivate = function (node) {
            var path = node.data.path;
            controller.modalSourcePathSelected = path;
            if (App.PageController.allowFolders || controller.isCsvPath(path)) {
                controller.enableElement("selectButton");
            } else {
                controller.disableElement("selectButton");
            }
        };

        controller.sourcePathSelectClicked = function () {
            controller.closeModalWindow();
            var path = controller.modalSourcePathSelected;
            var when = $("input:radio[name=whenOption]:checked").val();
            var doOpenCsv = when === "now" || controller.isCsvPath(path);
            controller.setCsvPath(path, doOpenCsv, doOpenCsv);
        };

        controller.clearDataTable = function () {
            App.DataTableController.clearDataTable();
            this.hasCsvOpen = false;
            this.hasResultsToClear = false;
        };

        controller.hasRequiredFields = function () {
            var isScheduled = this.isScheduled();
            return ((this.hasCsvOpen || isScheduled) && this.actionToPerform != null && this.actionToPerform !== "") && ((this.rootFileLoc != null && this.rootFileLoc !== "") || $("#myfile").val().length)
        };

        controller.updatePage = function () {
            BulkLoaderPageController.updatePage.call(this);
        };

        controller.processWhenChange = function () {
            BulkLoaderPageController.processWhenChange.call(this);
            var rootSourcePathLabel = $("#rootSourcePathLabel");

            if (controller.isNow()) {
                rootSourcePathLabel.html('CSV File');
                App.PageController.allowFolders = false;
            } else if (controller.isScheduled()) {
                rootSourcePathLabel.html('CSV File or Folder with CSV files');
                App.PageController.allowFolders = true;
            }

        };
        return controller;

    });
