define(["CsvPageController", "DataTableCsvController", "LoadValidateController"],
  function (CsvPageController, DataTableCsvController, LoadValidateController) {
    /*global App*/
    /*global log*/
    return function () {

      App.SourceViewController = App.DataTableController = new DataTableCsvController({columnHighlighting: true});
      App.ProcessController = new LoadValidateController();

      var controller = Object.create(CsvPageController);
      controller.pageType = "generatecsv";
      controller.sourceModalTital = "Select a Folder";
      controller.showSourceFilesSelect = false;
      controller.helpText = "Select a folder and then click 'Select'";
      controller.actionToPerform = "generatecsv";

      controller.disableActionButtons = function () {
        this.disableElement("loadButton");
        //this.hideElement("loadButton");
      };

      controller.enableActionButtons = function () {
         this.enableElement("loadButton");
      };

      controller.loadButtonClicked = function () {
        App.ProcessController.runValidation(App.PageController.serializeFormVals());
      };

      controller.hasRequiredFields = function () {
        var rSource = $('#rootSourcePath');
        return (rSource.length && rSource.val() && rSource.val().length);
      };

      controller.initPage = function (formVals, callback) {
        CsvPageController.initPage.call(this, formVals, callback);
       };

      controller.initFinalDisplayAdjust = function (formVals) {

        if (typeof formVals !== "undefined" && formVals["rootSourcePath"]) {
          var val = formVals["rootSourcePath"];
          controller.setSourcePath(val);
        }

        controller.showElement("containerOptionDiv");

        // finally update the page to ensure everything is set correctly
        controller.updatePage();
      };

      controller.sourceNodeModalActivate = function (node) {
        var path = node.data.path;
        controller.modalSourcePathSelected = path;
        controller.enableElement("selectButton");
      };

      controller.sourcePathSelectClicked = function () {
        controller.closeModalWindow();
        var path = controller.modalSourcePathSelected;
        controller.setSourcePath(path);
      };

      controller.updatePage = function () {
        CsvPageController.updatePage.call(this);
      };

      controller.processWhenChange = function () {
        $("#rootSourcePathLabel").html('Server Folder');
      };

      controller.setSourcePath = function(path) {
        $('#rootSourcePath').val(path);
        controller.updatePage();
      };

      return controller;
    }
  }
);