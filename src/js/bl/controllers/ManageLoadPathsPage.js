define(["RootPageController", "LoadPathModel", "DataTableCrudController"],
    function (RootPageController, LoadPathModel, DataTableCrudController) {
        return function() {
            /*global App*/

            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Load Path");
                App.DataTableController.setApiFuncs("datamanager.getLoadPaths","datamanager.addEditLoadPath", "datamanager.addEditLoadPath", "datamanager.removeLoadPath");
                App.DataTableController.initController(LoadPathModel, true);

                    // callback
                    if (callback){
                        callback();
                    }

            };

            controller.setBrowsePath = function (path) {

            };


            return controller;
        };
   });