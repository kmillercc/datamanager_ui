define(["BulkLoaderPageController", "DataTableCsvController", "TreeController", "LoadValidateController", "ProfileController", "JobScheduleController"],
    function (BulkLoaderPageController, DataTableCsvController, TreeController, BL_LoadValidateController, ProfileController, JobScheduleController) {
        /*global App*/
        /*global LL_URL*/
        return function () {

            App.DataTableController = new DataTableCsvController({noTableTools: true});
            App.SourceViewController = App.SourceTreeController = new TreeController("sourceDiv");
            App.TargetViewController = new TreeController("targetDiv");
            App.ProcessController = new BL_LoadValidateController();
            App.ProfileController = ProfileController;
            App.ProfileController.initPage();
            App.JobScheduleController = new JobScheduleController();
            App.JobScheduleController.initPage();

            var controller = Object.create(BulkLoaderPageController);
            controller.pageType = "structure";
            controller.sourceModalTital = "Select Load Directory";
            controller.showSourceFilesSelect = false;
            controller.helpText = "Select a folder and then click 'Select'";

            controller.initPage = function (formVals, callback) {
                BulkLoaderPageController.initPage.call(this, formVals, callback);

                // *** set load paths
                // *** load content into the modal
                //var progressHtml = '<div id="myModal"><img class="progress-img"></div>';

                $("#selectTargetPathButton").click(function (evt) {
                    controller.openModalWindow('<div id="myModal"><img class="progress-img"></div>', null, true, "modal_progress_indicator_div", false);
                    App.TargetPathSelectController = new TreeController("treeDiv_select", "myModal");
                    var html = App.TemplateUtils.getTemplate(App.browseSelectHtmlPath)({});
                    App.TargetPathSelectController.loadLLTreeInModal(ENT_WSPACE_ID, ENT_WSPACE_NAME, html, "Select Target Folder", false, controller.targetNodeModalActivate, controller.targetPathSelectClicked);
                });

            };

            controller.initFinalDisplayAdjust = function(formVals){
                // default in a target of enterprise if it wasn't already brought into the form
                if (typeof formVals === "undefined" || !formVals["rootTargetPath"]) {
                    controller.setTargetPath(ENT_WSPACE_ID, ENT_WSPACE_NAME, false);
                }

                // initialize action to "create" (first item in the pulldown)
                controller.hideShowAdvancedOptions('create');
            };

            controller.hasRequiredFields = function () {
                var rTarget = $('#rootTargetPath');
                var rSource = $('#rootSourcePath');
                return (rTarget.length && rSource.length && rTarget.val() && rTarget.val().length && rSource.val() && rSource.val().length && this.actionToPerform != null);
            };

            controller.updatePage = function () {
                BulkLoaderPageController.updatePage.call(this);
            };

            controller.sourcePathSelectClicked = function () {
                controller.closeModalWindow();
                controller.setRootSourcePath(this.modalSourcePathSelected);
            };

            controller.targetPathSelectClicked = function () {
                controller.closeModalWindow();
                controller.setTargetPath(controller.modalTargetIdSelected, controller.modalTargetPathSelected, true);
            };

            controller.hideShowAdvancedOptions = function (action) {
                // hide all
                $('div.advanced_settings > label').hide();

                // now just show ones for folder structure load
                $('div.advanced_settings > label.bl_structure').show();
            };


            controller.setRootSourcePath = function (path) {
                if (controller.isCsvPath(path)) {
                    var formVals = {rootSourcePath: path};
                    this.forwardToPage("bl", "loadcsv", formVals);
                }else{
                    $('#rootSourcePath').val(path);
                    controller.paramsChanged();

                    // *** check to make sure we are not over the limit
                    $.when(App.ServerAPI.getFileCount(path, false))
                        .done(function (num) {
                            if (controller.checkTotalNumItems(num)) {
                                controller.showLoadingIndicator("Processing");
                                controller.setEstimatedTotal(num);
                                controller.updatePage();
                                App.SourceTreeController.loadFileTree(path, true, false, false, null, [
                                    //{title: "View Metadata", cmd: "viewmetadata"},
                                    {title: "Load from here", cmd: "setloadsource"},
                                    {title: "Refresh", cmd: "setloadsource"},
                                    {title: "Show file path", cmd: "showfilepath"},
                                    {title: "Exclude this node from the Load", cmd: "skipnode"},
                                    {title: "Include this node in the load", cmd: "includenode"}
                                ], false, function () {
                                    controller.hideLoadingIndicator();
                                });
                            }
                        })
                        .fail(function (errMsg) {
                            controller.showError(errMsg);
                        });
                }

            };

            controller.setTargetPath = function (dataId, path, doUpdate) {
                $('#rootTargetPath').val(path);
                controller.rootTargetId = dataId;
                controller.rootTargetPath = path;
                App.TargetViewController.loadLLTree(dataId, path, true, false, null, [
                    {title: "Set as Target Container", cmd: "setloadtarget"},
                    {title: "Refresh", cmd: "setloadtarget"},
                    {title: "Export from here", cmd: "exportfromhere"}
                ]);
                if (doUpdate) {
                    controller.paramsChanged();
                    controller.updatePage();
                }
            };

            controller.targetNodeModalActivate = function (node) {
                if (node.data.unselectable) {
                    controller.disableElement("selectButton");
                    controller.modalTargetIdSelected = 0;
                    controller.modalTargetPathSelected = '';
                } else {
                    controller.enableElement("selectButton");
                    controller.modalTargetIdSelected = node.data.dataid;
                    controller.modalTargetPathSelected = node.data.path;
                }
            };

            controller.sourceNodeModalActivate = function (node) {
                controller.enableElement("selectButton");
                controller.modalSourcePathSelected = node.data.path;
            };

            controller.showMetadata = function (path, metadata) {
                if (!metadata) {
                    App.PageController.showError("No metadata found for " + path);
                    // clear the table
                    App.DataTableController.clearDataTable();
                    return;
                }
                var data = metadata.data;
                var colNames = metadata.colNames;
                if (colNames.length) {
                    // *** have to do this twice to get columns to line up
                    App.DataTableController.receiveDataFromDesktop(colNames, data, false);
                    if (data.length > 1) {
                        $("#gridPopupDiv").dialog(
                            { autoOpen: true, width: 900, height: 600 }
                        );
                        App.DataTableController.refreshColumns(data);
                    } else {
                        var editor = App.DataTableController.editor;
                        editor.edit(
                            $("#grid > tbody > tr")[0],
                            'View Metadata'/*,
                             { "label": "Update", "fn": function () {
                             editor.submit()
                             } }*/
                        );
                    }
                }
                else {
                    App.PageController.showError("No metadata found for " + path);
                    // clear the table
                    App.DataTableController.clearDataTable();
                }
            };

            return controller;
        };
    });
