define(["RootPageController", "LoadProfileModel", "DataTableCrudController"],
    function (RootPageController, LoadProfileModel, DataTableCrudController) {
        return function() {
            /*global App*/
            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Load Profile");
                App.DataTableController.setApiFuncs("datamanager.getLoadProfiles","datamanager.addEditLoadProfile", "datamanager.addEditLoadProfile", "datamanager.removeLoadProfile");
                App.DataTableController.initController(LoadProfileModel, false, null, null, 'profileId');

                    // callback
                    if (callback){
                        callback();
                    }
            };


            return controller;
        };
   });