define(["CsvPageController", "ProfileController", "JobScheduleController", "DataTableCsvController", "LoadValidateController"],
    function (CsvPageController, ProfileController, JobScheduleController, DataTableCsvController, LoadValidateController) {
        /*global App*/
        /*global LL_URL*/
        return function () {

            App.SourceViewController = App.DataTableController = new DataTableCsvController({columnHighlighting: true});
            App.ProcessController = new LoadValidateController();
            App.ProfileController = ProfileController;
            App.ProfileController.initPage();
            App.JobScheduleController = new JobScheduleController();
            App.JobScheduleController.initPage();


            var controller = Object.create(CsvPageController);

            controller.initPage = function (formVals, callback) {
                CsvPageController.initPage.call(this, formVals, callback);
                controller.setAction("createusergroup");
            };

            controller.initFinalDisplayAdjust = function(formVals){
                // initialize action to "create" (first item in the pulldown)
                controller.setAction('createusergroup', true);

                if (typeof formVals !== "undefined" && formVals["rootSourcePath"]) {
                    var val = formVals["rootSourcePath"];
                    controller.openServerCsv(val, false, 10, false);
                }

                controller.hideShowAdvancedOptions("users");
            };


            controller.setAction = function (action) {
                $("#action").val(action);
                controller.actionToPerform = action;
                controller.hideShowNameConflictDiv(action);
                controller.hideShowMemberUpdateDiv(action);
                controller.hideShowMemberAddDiv(action);
            };

            controller.hideShowNameConflictDiv = function (action) {
                if (action === "createusergroup") {
                    controller.showElement("nameCollisionFieldset");
                } else {
                    controller.hideElement("nameCollisionFieldset");
                }
            };

            controller.hideShowMemberUpdateDiv = function (action) {
                if (action === "updateusergroup") {
                    controller.showElement("updateMembersControlGroup");
                }
                else {
                    controller.hideElement("updateMembersControlGroup");
                }
            };

            controller.hideShowMemberAddDiv = function (action) {
                if (action === "createusergroup") {
                    controller.showElement("addMembersControlGroup");
                } else {
                    controller.hideElement("addMembersControlGroup");
                }
            };

            controller.setActionFromCsv = function (colNames) {
                var currentAction = App.PageController.actionToPerform;

                if (App.ArrayUtils(colNames).contains("$ID", true)) {
                    if (currentAction !== "updateusergroup") {
                        App.PageController.setAction("updateusergroup", true);
                        controller.showInfo("Please note: the operation has been set to 'Update' based on the presence of the '$ID' column in the CSV.", true);
                    }

                } else {
                    if (currentAction !== "createusergroup") {
                        App.PageController.setAction("createusergroup", true);
                        controller.showInfo("Please note: the operation has been set to 'Create' based on the absence of the '$ID' column in the CSV.", true);
                    }
                }

            };

            return controller;
        };
    });
