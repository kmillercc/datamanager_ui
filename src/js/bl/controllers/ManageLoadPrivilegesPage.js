define(["RootPageController", "LoadPrivilegeModel", "DataTableCrudController"],
    function (RootPageController, LoadPrivilegeModel, DataTableCrudController) {
        return function() {
            /*global App*/
            App.DataTableController = new DataTableCrudController();


            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("User");
                App.DataTableController.setApiFuncs("datamanager.getLoadUsers","datamanager.addEditLoadUser", "datamanager.addEditLoadUser", "datamanager.removeLoadUser");
                App.DataTableController.initController(LoadPrivilegeModel, true, null, true, 'userName');

                    // callback
                    if (callback){
                        callback();
                    }
            };


            return controller;
        };
   });