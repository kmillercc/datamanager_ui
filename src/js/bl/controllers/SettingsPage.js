define(["BulkLoaderPageController"],
    function (BulkLoaderPageController) {
        return function () {
            var controller = Object.create(BulkLoaderPageController);
            controller.initPage = function (formVals, callback) {
                BulkLoaderPageController.initPage.call(this, formVals);

                // callback
                if (callback) {
                    callback();
                }
            };

            return controller;
        }
    });