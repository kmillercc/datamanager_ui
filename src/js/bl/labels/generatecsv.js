define({
    "page_title" : "Generate CSV File from Folder Structure",
    "action_buttons": [
        {id : "loadButton", className: "btn btn-small btn-primary btn-action", label: "Generate CSV"},
    ],
    grid_label: "Metadata View",
    source_label: "Source View",
    help_block: "This page is for generating a CSV file from a Folder Structure"
});
