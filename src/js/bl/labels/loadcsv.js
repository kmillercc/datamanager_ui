define({
    "page_title" : "Load from CSV",
    "action_buttons": [
        {id : "validateButton", className: "btn btn-small btn-primary", label: "Validate"},
        {id : "loadButton", className: "btn btn-small btn-primary btn-action", label: "Load"},
        {id : "scheduleButton", className: "btn btn-small btn-primary", label: "Save Scheduled Job"}
    ],
    grid_label: "Source View"
});