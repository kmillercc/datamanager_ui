define(["async", "StopWatch", "lodash"],
  function (async, StopWatch, _) {
    /*global App*/
    return {
      jobId: 0,
      nodeIdArray: [],
      nodeObjects: {},
      NONFATAL_ERROR_PERCENT_LIMIT: 25,
      EVAL_LIMIT: 0,
      errorNodes: [],
      hasResultsToClear: false,
      showLimitReached: false,
      isCancelled: false,
      exit: false,
      timeoutId: undefined,
      itemsInitialized: 0,
      itemsToProcess: 0,
      stopWatch: new StopWatch(),
      lineErrorMap: {},
      numErrors: 0,
      allowSmallerChunks: true,
      runSingleThreaded: false,

      resetProcess: function (doClearResults) {
        if (this.hasResultsToClear && doClearResults) {
          App.SourceViewController.clearResults(this.errorNodes);
        }
        this.errorNodes = [];
        this.nodeIdArray = [];
        this.nodeObjects = {};
        this.EVAL_LIMIT = 0;
        this.isCancelled = false;
        this.timeoutId = undefined;
        this.itemsInitialized = 0;
        this.itemsToProcess = 0;
        this.lineErrorMap = {};
        this.numErrors = 0;
        this.jobId = 0;
        this.exit = false;
        this.hasResultsToClear = false;
        this.allowSmallerChunks = true;
        this.runSingleThreaded = false;
        App.PageController.setInvalidForLoad();
      },

      skipNode: function (id, path) {
        // TODO complete this code
      },

      includeNode: function (id, path) {
        // TODO complete this code
      },

      getNodes: function (startNo, endNo) {
        var retObj = {};
        for (var i = startNo; i <= endNo; i++) {
          retObj[i] = this.nodeObjects[i];
        }
        return retObj;
      },

      setInvalidForLoad: function () {
        // just set the jobId to 0
      },

      exitUICallback: function () {
        App.SourceViewController.exitUICallback();
      },

      processSuccessNode: function (node, text) {
        node["ok"] = true;
        this.nodeObjects[node.id] = node;
        this.hasResultsToClear = true;
        App.SourceViewController.showSuccessNode(node["id"], node["path"], text);
      },

      processSkippedNode: function (node, text) {
        node["skip"] = true;
        this.nodeObjects[node.id] = node;
        this.hasResultsToClear = true;
        App.SourceViewController.showSkippedNode(node["id"], node["path"], text);
      },

      processErrorNode: function (node, errMsg) {
        node["errMsg"] = errMsg;
        node["ok"] = false;
        this.hasResultsToClear = true;
        this.nodeObjects[node.id] = node;
        App.SourceViewController.showErrorNode(node["id"], node["path"], errMsg);
        this.errorNodes.push({ "id": node["id"], "path": node["path"] });
      },


      showLoadProgress: function (progressBar, jobId, errorFunc, context) {
        var completedCount = 0;
        var that = this;
        async.whilst(
          function () {
            if (that.exit) {
              return false;
            } else {
              return true;
            }
          },
          function (callback) {
            $.when(App.ServerAPI.checkJobStatus(jobId, context))
              .done(function (result) {
                if (that.isCancelled) {
                  that.exit = true;
                  callback(null);
                  log("cancelled")
                } else if (result["completed"]) {
                  that.exit = true;
                  callback(null);
                } else {
                  var newCount = result["completedCount"];
                  var increment = newCount - completedCount;
                  completedCount = newCount;
                  if (increment) {
                    progressBar.update(increment);
                  }
                  that.timeoutId = setTimeout(callback, 3000);
                }
              })
              .fail(function (errMsg) {
                callback(errMsg);
              });
          },
          function (errMsg) {
            if (errMsg) {
              clearTimeout(that.timeoutId);
              errorFunc(errMsg);
              that.exit = true;
            }
          }
        );
      },

      showInitializeProgress: function (jobId, errorFunc, nonFatalErrorFunc, successFunc, progressMsg, context, showInitTimings, successMsg) {
        var completedCount = 0;
        var that = this;
        var progressBar = new App.PageController.ProgressBar(progressMsg, App.PageController.getEstimatedTotal());
        progressBar.show(true);

        async.whilst(
          function () {
            if (that.exit) {
              progressBar.finish();
              clearTimeout(that.timeoutId);

              // show success or error
              var lineErrorMap = that.lineErrorMap;
              //log(lineErrorMap);
              _.forEach(that.nodeObjects, function (nodeObject) {
                var id = nodeObject["id"];
                var errMsg = lineErrorMap[id];
                if (errMsg) {
                  that.nonFatalErrors += 1;
                  that.processErrorNode(nodeObject, errMsg);
                } else {
                  that.processSuccessNode(nodeObject, successMsg);
                }
              }, that);

              if (that.numErrors > 0) {
                nonFatalErrorFunc({ "nonFatalErrors": that.numErrors });
              } else if (!that.isCancelled) {
                successFunc();
              }
              that.exitUICallback();
              return false;
            } else {
              return true;
            }
            // test
            // return true if all is ok
            // return false to abort
          },
          function (callback) {
            $.when(App.ServerAPI.checkJobStatus(jobId, context))
              .done(function (result) {
                if (that.isCancelled) {
                  that.exit = true;
                  callback(null);
                  log("cancelled")
                } else if (result["completed"]) {
                  progressBar.finish();
                  context.nodeIdArray = result["nodeIdArray"];
                  context.nodeObjects = result["nodeObjects"];
                  context.itemsInitialized = result["itemsInitialized"];
                  context.itemsToProcess = result["itemsToProcess"];
                  context.numErrors = result["numErrors"];
                  context.lineErrorMap = result["lineErrorMap"];

                  // set a flag to indicate whether to allow chunking
                  context.allowSmallerChunks = result["allowSmallerChunks"];


                  if (showInitTimings) {
                    //log("Initialize Timings:");
                    //log("   " + result["elapsed"] + " elapsed time");
                    //log("   " + result["average"] + " seconds per item");
                  }
                  that.exit = true;
                  callback(null);
                } else {
                  var newCount = result["completedCount"];
                  var increment = newCount - completedCount;
                  completedCount = newCount;
                  if (increment) {
                    progressBar.update(increment);
                  }
                  that.timeoutId = setTimeout(callback, 3000);
                }
              })
              .fail(function (errMsg) {
                callback(errMsg);
              });
          },
          function (errMsg) {
            if (errMsg) {
              clearTimeout(that.timeoutId);
              errorFunc(errMsg);
              that.exit = true;
            }
            that.exitUICallback();
          }
        );
      },

      initializeCsvGeneration: function (optionStr, context) {
        var dfd = $.Deferred();

        // *** reset the process
        this.resetProcess(true);

        // get any source settings (like hidden vals, push down vals, etc)
        var settings = App.SourceViewController.getSourceSettings();

        // show generic loading indicator
        App.PageController.showLoadingIndicator("Generating", false);

        // add the jobId to optionStr
        optionStr += "&jobId=" + this.jobId;
        $.when(App.ServerAPI.initializeProcess(App.PageController.actionToPerform, [], optionStr, settings))
          .done(function (result) {
            log(result);
            App.PageController.hideLoadingIndicator();
            context.EVAL_LIMIT = result["limit"];
            context.jobId = result["jobId"];
            //that.runSingleThreaded = result["runSingleThreaded"];
            log("Set jobID");
            dfd.resolve(result);
          })
          .fail(function (errMsg) {
            App.PageController.hideLoadingIndicator();
            dfd.reject(errMsg);
          });

        return dfd.promise();

      },

      initializeExport: function (optionStr, context) {
        var that = this;
        var dfd = $.Deferred();

        // *** reset the process
        this.resetProcess(true);

        // get any source settings (like hidden vals, push down vals, etc)
        var settings = App.SourceViewController.getSourceSettings();

        // *** get the total
        var total = App.PageController.getEstimatedTotal();

        // show generic loading indicator
        App.PageController.showLoadingIndicator("Initializing", false);

        // add the jobId to optionStr
        optionStr += "&jobId=" + this.jobId + "&estimatedTotal=" + total;
        $.when(App.ServerAPI.runExport(App.PageController.actionToPerform, App.PageController.getColNames(), optionStr, settings))
          .done(function (result) {
            log(result);
            App.PageController.hideLoadingIndicator();
            context.EVAL_LIMIT = result["limit"];
            context.jobId = result["jobId"];
            that.runSingleThreaded = result["runSingleThreaded"];
            log("Set jobID");
            dfd.resolve(result);
          })
          .fail(function (errMsg) {
            App.PageController.hideLoadingIndicator();
            dfd.reject(errMsg);
          });

        return dfd.promise();

      },

      initializeLoad: function (optionStr, context) {
        var that = this;
        var dfd = $.Deferred();

        // *** reset the process
        this.resetProcess(true);

        // get any source settings (like hidden vals, push down vals, etc)
        var settings = App.SourceViewController.getSourceSettings();

        // *** get the total
        var total = App.PageController.getEstimatedTotal();

        // show generic loading indicator
        App.PageController.showLoadingIndicator("Initializing", false);

        // add the jobId to optionStr
        optionStr += "&jobId=" + this.jobId + "&estimatedTotal=" + total;
        $.when(App.ServerAPI.initializeLoad(App.PageController.actionToPerform, App.PageController.getColNames(), optionStr, settings))
          .done(function (result) {
            log(result);
            App.PageController.hideLoadingIndicator();
            context.EVAL_LIMIT = result["limit"];
            context.jobId = result["jobId"];
            that.runSingleThreaded = result["runSingleThreaded"];
            log("Set jobID");
            dfd.resolve(result);
          })
          .fail(function (errMsg) {
            App.PageController.hideLoadingIndicator();
            dfd.reject(errMsg);
          });

        return dfd.promise();

      },

      cancel: function () {
        this.isCancelled = true;
      },

      hasReachedErrorLimit: function (total, num) {
        var limit = this.NONFATAL_ERROR_PERCENT_LIMIT;
        return ((num / total) * 100) > limit;
      },

      iterateNodes: function (action, jobId, successFunc, fatalErrFunc, nonFatalErrFunc, nodeArray, progressBar, apiFunc, useErrorLimit, result) {
        result = (typeof result === "undefined") ? { nonFatalErrors: 0, ok: true, errMsg: null } : result;
        useErrorLimit = (typeof useErrorLimit === "undefined") ? true : useErrorLimit;

        this.exit = false;
        this.isCancelled = false;
        var that = this;
        var i = -1;

        // show progress bar
        progressBar.show(true);

        async.whilst(
          function () {
            if (that.exit) {
              that.exitUICallback();
              return false;
            } else {
              return true;
            }
            // test
            // return true if all is ok
            // return false to abort
          },
          function (callback) {
            i++;
            var chunk = nodeArray[i];

            if (that.runSingleThreaded) {
              that.showLoadProgress(progressBar, jobId, fatalErrFunc, this);
            }

            $.when(that.processChunkAsync(apiFunc, action, jobId, chunk, result))
              .done(function (rtn) {
                if (useErrorLimit && that.hasReachedErrorLimit(progressBar.total, result.nonFatalErrors)) {
                  callback("Error Limit Reached: More than " + that.NONFATAL_ERROR_PERCENT_LIMIT + "% of the load has errors. Cancelling the rest of job.");
                  //log("reached error limit")
                } else if (progressBar.numDone > that.EVAL_LIMIT) {
                  callback("You've reached the Evaluation limit for this job. To load or export more items, please purchase a license");
                  //log("reached evalLimit")
                } else if (that.isCancelled) {
                  that.exit = true;
                  callback(null);
                  log("cancelled")
                } else {
                  progressBar.update(chunk.length);
                  if (progressBar.isFinished()) {
                    progressBar.finish();
                    if (result.nonFatalErrors > 0) {
                      nonFatalErrFunc(result)
                    } else if (result.ok) {
                      successFunc();
                    }
                    that.exit = true;
                  }
                  callback(null);
                }
              })
              .fail(function (errMsg) {
                // we have an error. call the callback to exit out of loop
                callback(errMsg);
              });
          },

          function (errMsg) {
            if (errMsg) {
              fatalErrFunc(errMsg);
              that.exitUICallback();
              that.exit = true;
            }
          }
        );

      },


      actionWrapper: function (apiFunc, action, nodeObjects, jobId, context) {
        var dfd = $.Deferred();
        $.when(apiFunc(action, jobId, nodeObjects, context))
          .done(function (nodeObjects) {
            dfd.resolve(nodeObjects);
          })
          .fail(function (errMsg, context) {
            dfd.reject(errMsg, context);
          });
        return dfd.promise();
      },

      getNodeData: function (id) {
        var node = this.nodeObjects[id];
        var path = (node["path"]) ? node["path"] : "unknown";
        var isCsv = path.slice(-3).toUpperCase() === "CSV";
        node.skip = node["skip"] || isCsv;

        return node;

      },

      processChunkAsync: function (apiFunc, action, jobId, chunk, result) {
        var dfd = $.Deferred();
        var that = this;

        if (chunk == null || typeof chunk === 'undefined') {
          dfd.reject("An unknown error has occurred. Please try again.")
        }

        var chunks = [chunk];
        if (this.allowSmallerChunks) {
          // divide the chunk into smaller chunks to be able to process 6 requests simultaneously
          chunks = _.chunk(chunk, 6);
        }

        async.forEach(chunks,
          function (chunk, callback) {
            var nodeObjects = [];
            _.forEach(chunk, function (id) {
              var nodeData = that.getNodeData(id);
              if (!nodeData.skip) {
                nodeObjects.push(nodeData);
              } else {
                if (nodeData.errMsg.length) {
                  //do nothing, there is an error already shown that we should not cover up
                } else {
                  that.processSkippedNode(id, "Skipped");
                }
              }
            }, that);

            $.when(that.actionWrapper(apiFunc, action, nodeObjects, jobId, that))
              .done(function (results) {
                _.forEach(results, function (nodeObject) {
                  if (nodeObject["ok"]) {
                    that.processSuccessNode(nodeObject, nodeObject["successMsg"]);
                  } else {
                    result.nonFatalErrors += 1;
                    that.processErrorNode(nodeObject, nodeObject["errMsg"]);
                  }
                }, that);
                callback(null)
              })
              .fail(function (errMsg) {
                callback(errMsg);
              });

          }, function (errMsg) {
            if (errMsg) {
              // this is a fatal error that will abort the whole process
              dfd.reject(errMsg);
            } else {
              dfd.resolve(true);
            }
          });


        return dfd.promise();
      }


    }


  });
