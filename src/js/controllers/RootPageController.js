define(['bootbox', 'StopWatch', "combobox", "popup", "ProgressBar"],
  function (bootbox, StopWatch, combobox, popup, ProgressBar) {
    /*global App*/
    /*global LOAD_EXPORT_SYNC_LIMIT*/
    return {
      filePath: null,
      folderPath: null,
      gridDivId: "grid_div",
      estimatedTotal: 0,
      pageType: '',
      actionToPerform: '',
      hasResultsToClear: false,
      profileModel: null,
      colNames: [],
      hasCSVChanges: false,
      initPage: function () {

        //$("span.custom-combobox").hide();

        // do all the accordion elements
        $("#advancedDiv").accordion({
          collapsible: true,
          active: false,
          heightStyle: 'content'
        });
        $("#nameCollisonDiv").accordion({
          collapsible: true,
          active: false,
          heightStyle: 'content'
        });
        // hide the border on grid so it doesn't look funny
        $("#grid_div").addClass("hideBorder");

        $('#progressDiv').modal({
          keyboard: false,
          backdrop: false,
          show: false
        });

        $('input[type="radio"][name="whenOption"]').click(function (e) {
          App.PageController.processWhenChange();
          App.PageController.updatePage();
        });

        var sourceId = this.getParameterByName("sourceId");
        if (sourceId) {
          if ((App.PageName === "exportobjects" || App.PageName === "exportmetadata") && App.PageController.setRootSourcePath) {
            $.when(App.ServerAPI.getPathFromId(sourceId))
              .done(function (path) {
                App.PageController.setRootSourcePath(path, sourceId);
              })
              .fail(function (errMsg) {
                App.PageController.showError(errMsg);
              });
          }
        }

        var targetId = this.getParameterByName("targetId");
        if (targetId) {
          if ((App.PageName === "loadfilestructure" || App.PageName === "loadcsv") && App.PageController.setTargetPath) {
            $.when(App.ServerAPI.getPathFromId(targetId))
              .done(function (path) {
                App.PageController.setTargetPath(targetId, path, false);
              })
              .fail(function (errMsg) {
                App.PageController.showError(errMsg);
              });
          }
        }

        // formVals may have set async option
        App.PageController.processWhenChange();

        // disable action buttons
        App.PageController.disableActionButtons(true);

      },
      setFormVals: function (formValsIn) {

        // uncheck all checkboxes - if saved values have the fields checked, they will get checked below
        $("checkbox").prop('checked', false);

        // make a copy of the formVals
        var formVals = $.extend({}, formValsIn);

        // set form vals into page if we have them
        var p = App.PageController;

        //log("Pass 1:");
        var alreadySet = {};
        // get the inputs in this form and set from formVals
        _.each($("input,select,textarea"), function (el) {
          if (!alreadySet[el.name]) {
            var val = formVals[el.id] || formVals[el.name];
            if (typeof val !== 'undefined') {
              alreadySet[el.name] = true;
              p.setFormVal($(el), val, el.name);
              //delete formVals[el.id];
              //delete formVals[el.name];
            }
          }
        });


        var keys = _.keys(formVals);
        _.each(keys, function (name) {
            if (name.substr(0, 14) === "daily_at_time_") {
              var inputObj = $("#" + name);
              if (!inputObj.length) {
                log("setting " + name);
                App.JobScheduleController.addDailyAddTime(name, formVals[name]);
              }
            }
        });


      },

      setFormVal: function (inputObj, val, name) {
        //log("setting " + name + " to value " + val);
        // is there a setter for this field?  If so, call it

        var setter = "set" + App.PageController.initCap(name);
        //log(setter + " " + val);
        var setterFunc = App.PageController[setter];

        if (name !== "action" && setterFunc && val != null && typeof val !== 'undefined' && val.length) {
          setterFunc(val, true);
        }
        else {
          switch (inputObj.prop('tagName')) {
            case 'INPUT':
              switch (inputObj.attr('type')) {
                case 'hidden':
                case 'text':
                case 'number':
                  inputObj.val(val);
                  break;
                case 'time':
                case 'date':
                case 'datetime':
                  if (val != null && typeof val !== 'undefined' && val.length) {
                    inputObj.val(val);
                  }
                  break;
                case 'checkbox':
                  if (val && val !== 'false' && val !== '') {
                    inputObj.prop('checked', true);
                  }
                  else {
                    inputObj.prop('checked', false);
                  }
                  break;
                case 'radio':
                  inputObj.prop('checked', false);
                  $("input[name='" + name + "'][value='" + val + "']").prop('checked', true);
                  break;
              }
              break;
            case 'SELECT':
              inputObj.val(val);
              break;
            case 'TEXTAREA':
              inputObj.val(val);
              break;
            default:
              inputObj.val(val);
              break;
          }
          if (name === "profileId") {
            App.ProfileController.setProfileFromInitPage(val);
          } else if (name === "whenOption") {
            App.PageController.processWhenChange();
          } else if (name === "action") {
            App.PageController.setAction(val, false);
          }
        }
      },

      isPositiveInteger: function (val) {
        var n = ~~Number(val);
        return String(n) === val && n >= 0;
      },

      getParameterByName: function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
      },
      initCap: function (s) {
        return s.charAt(0).toUpperCase() + s.slice(1);
      },
      setAction: function (action) {
        // implemented in subclasses
      },
      getColNames: function () {
        return this.colNames;
      },

      profileChanged: function (formVals, profile) {
        // implemented in subclassses
      },

      getHtmlAndReplace: function (divId, funcToCall, formVals, callback) {
        App.PageController.hideElement(divId);
        var div = $("#" + divId);
        $.when(funcToCall(formVals))
          .done(function (htmlStr) {
            div.html(htmlStr);
            App.PageController.showElement(divId);
            if (callback) {
              callback();
            }

          })
          .fail(function (errMsg) {
            App.PageController.showError(errMsg);
          });
      },

      setSelectOptions: function (valKey, displayKey, selectId, data, set1stValue, callback) {
        var selectBox = $("#" + selectId);
        _.each(data, function (o) {
          selectBox.append($("<option>")
            .val(o[valKey])
            .html(o[displayKey])
            .attr('data', JSON.stringify(o))
          );
        }, this);
        var children = selectBox.children();
        if (children.length && set1stValue) {
          selectBox.val(children[0].value);
          selectBox.trigger("change");
        }
        if (callback) {
          callback();
        }

      },

      getSetSelectOptions: function (model, valKey, displayKey, selectId, funcToCall, params, set1stValue, callback) {
        App.PageController.hideElement(selectId);
        var selectBox = $("#" + selectId);
        $.when(funcToCall(model, params, false))
          .done(function (data) {
            _.each(data, function (o) {
              selectBox.append($("<option>")
                .val(o[valKey])
                .html(o[displayKey])
                .attr('data', JSON.stringify(o))
              );
            }, this);
            App.PageController.showElement(selectId);
            var children = selectBox.children();
            if (children.length && set1stValue) {
              selectBox.val(children[0].value);
              selectBox.trigger("change");
            }
            if (callback) {
              callback();
            }

          })
          .fail(function (errMsg) {
            App.PageController.showError(errMsg);
          });
      },
      forwardToPage: function (module, page, formVals, callback) {
        App.Router.switchPage(module, page, formVals, function () {

          // push this page onto the window history to make everything look right
          var html = $('html').html();
          var pageTitle = document.title;
          var location = window.location;
          var url = location.href.replace(location.hash, "#" + module + "/" + page);

          window.history.pushState({ "html": html, "pageTitle": pageTitle }, "", url);

          // KM 7-26-16 put this in for IE
          //this.removeListener();
          //location.hash = "#" + module + "/" + page;
          //this.addListener()

          if (callback) {
            callback();
          }
        });
      },
      /*
      addListener: function () {
          if (w.addEventListener) {
              w.addEventListener('hashchange', App.routie.hashChanged, false);
          } else {
              w.attachEvent('onhashchange', hashChanged);
          }
      },

      removeListener: function () {
          if (w.removeEventListener) {
              w.removeEventListener('hashchange', hashChanged);
          } else {
              w.detachEvent('onhashchange', hashChanged);
          }
      },
      */
      isCsvPath: function (path) {
        return path.substring(path.length - 3).toUpperCase() === "CSV"
      },
      isTextFile: function (node) {
        //log(path.substring(path.length - 4,path.length - 3));
        var path = node.data.path;
        return !node.data["isFolder"] && (path.substring(path.length - 3).toUpperCase() === "CSV" || path.substring(path.length - 3).toUpperCase() === "TXT");
      },
      isUserCsv: function (path) {
        var isUser = _.contains(path.toUpperCase(), "USER");
        var isGroup = _.contains(path.toUpperCase(), "GROUP");
        return isUser || isGroup;
      },
      isFolderPath: function (path) {
        return path.substring(path.length - 1) === "/" || path.substring(path.length - 1) === "\\";
      },
      serializeFormVals: function (getScheduleForm) {
        var rootSourcePath = $("#rootSourcePath");
        var rootSourcePathVal = rootSourcePath.val();
        if (rootSourcePathVal) {
          rootSourcePath.val(rootSourcePathVal.replace("&", ";amp;"));
        }
        var s = $("#myform").serialize() + "&" + $("#myform_adv").serialize();
        if (getScheduleForm) {
          var scheduleForm = $("#schedule_form");
          s += scheduleForm.length ? "&" + scheduleForm.serialize() : '';
        }
        return s;
      },
      enableElement: function (id) {
        $("#" + id).prop('disabled', false);
      },
      disableElement: function (id) {
        $("#" + id).prop('disabled', true);
      },
      hideElement: function (id) {
        $("#" + id).hide();
      },
      showElement: function (id) {
        $("#" + id).show();
      },
      showGrid: function () {
        this.showElement(this.gridDivId);
      },
      hideGrid: function () {
        this.hideElement(this.gridDivId);
      },
      reloadGrid: function () {

      },
      gridLoadDone: function () {
        $("#grid_div").removeClass("hideBorder");
        App.PageController.showGrid();
      },
      showError: function (msg) {
        popup._alert(msg, "alert-error");
      },
      showWarning: function (msg) {
        popup._alert(msg, "alert-warning");
      },
      showInfo: function (msg, autoClose) {
        popup._alert(msg, "alert-info", autoClose);
      },
      showSuccess: function (msg) {
        popup._success(msg, "alert-success");
      },
      showLoadingIndicator: function (title, backdrop, clockSeconds) {
        title = title || "Processing";
        var progressBar = new ProgressBar(title, 0, clockSeconds);
        progressBar.show();
        return progressBar;
      },
      confirm: function (question, callback) {
        popup._confirm(question, function (result) {
          //defer the execution so we can immediately return (avoid having the dialog hanging on screen)
          setTimeout(function () {
            callback(result)
          }, 100);
          return true;
        }, "Confirm");
      },
      hideLoadingIndicator: function () {
        popup._hideAlerts();
      },

      dataTableCallback: function () {
        // button to update job status
        $(".jobstatus_refresh").each(function () {
          var id = $(this).attr('id');
          var scheduleId = id.substr(id.indexOf("_") + 1);
          $("#" + id).click(function (e) {
            var lastRunTD = $("#lastrun_" + scheduleId);
            var nextRunTD = $("#nextrun_" + scheduleId);
            var statusDiv = $("#jobstatus_" + scheduleId);
            statusDiv.html('<div id="mybar" class="progress-img" style="width: 50%; text-align: center"></div>');
            $.when(App.ServerAPI.getJobStatusHtml(scheduleId))
              .done(function (result) {
                statusDiv.html(result["statusHtml"]);
                nextRunTD.html(result["nextRunTime"]);
                lastRunTD.html(result["lastRunTime"]);
              })
              .fail(function (errMsg) {
                log(errMsg);
                statusDiv.html('');
              });
          });
        });
      },
      checkFilePath: function (filePath, showError, successCallback, errorCallback) {
        $.when(App.ServerAPI.checkFilePath(filePath, this))
          .done(function (result, context) {
            if (successCallback) {
              successCallback(filePath)
            }
          })
          .fail(function () {
            if (errorCallback) {
              errorCallback(showError)
            }
          });
      },
      checkFolderPath: function (folderPath, showError, successCallback, errorCallback) {
        $.when(App.ServerAPI.checkFolderPath(folderPath, this))
          .done(function (result, context) {
            if (successCallback) {
              successCallback(folderPath)
            }
          })
          .fail(function () {
            if (errorCallback) {
              errorCallback(showError)
            }
          });
      },
      closeModalWindow: function () {
        popup._hideAlerts();
      },
      openModalWindow: function (msg, title, closeButton, className, buttons, height, width, callback) {
        popup._dialog(msg, title, closeButton, className, false, false, buttons);
        var bb = $(".bootbox");
        if (height) {
          bb.css("height", height);
        }
        if (width) {
          bb.css("width", width);
        }
        if (callback) {
          callback();
        }
      },
      updateModalWindow: function (rootDivId, msg, title, height, width) {
        $(".bootbox").css("height", height).css("width", width);
        $('#' + rootDivId).html(msg);
        $('.modal-header').html(title);
      },
      updatePopupWindow: function (rootDivId, msg, title, height, width) {
        $(".bootbox").css("height", height).css("width", width);
        $('#' + rootDivId).html(msg);
        $('.modal-header').html(title);
      },
      hideAlerts: function () {
        popup._hideAlerts();
      },
      openModalForm: function (formName, title, pageTemplate, height, width, saveCallback, closeCallback) {
        var baseTemplate = App.TemplateUtils.getTemplate(App.formModalHtmlPath);
        App.TemplateUtils.registerPartial("page_content1", pageTemplate);
        var html = baseTemplate({ form_id: formName, form_name: formName });
        var buttons = {
          save: {
            label: "Save",
            className: "btn-primary",
            callback: saveCallback
          },
          cancel: {
            label: "Cancel",
            className: "btn",
            callback: closeCallback
          }
        };
        this.openModalWindow(html, title, true, "modal_form", buttons, height, width);
      },

      hasRequiredFields: function () {
        return false;
      },

      areAnyChecked: function (className) {
        return !!$('.' + className + ':checked').length;
      },

      enableActionButtons: function () {
        // implemented in subclasses
      },

      disableActionButtons: function () {
        // implemented in subclasses
      },

      updatePage: function () {
        if (App.PageController.hasRequiredFields()) {
          App.PageController.enableActionButtons();
        } else {
          App.PageController.disableActionButtons(true);
        }
      },
      isNow: function () {
        var when = $("input:radio[name=whenOption]:checked").val();
        return when === "now"
      },
      isScheduled: function () {
        var when = $("input:radio[name=whenOption]:checked").val();
        return when === "async"
      },
      processWhenChange: function () {
        var when = $("input:radio[name=whenOption]:checked").val();
        switch (when) {
          case "now":
            this.hideElement("scheduleDiv");
            this.hideElement("scheduleButton");
            this.showElement("exportButton");
            this.showElement("targetContainer");
            this.showElement("sourceContainer");
            break;
          case "async":
            if (App.PageController.hasCSVChanges) {
              this.showWarning('Note: To allow the Loader to pick up changes you have made to the CSV, you must first save the CSV to the load directory (Click "Save to File").');
            }
            this.showElement("scheduleDiv");
            this.showElement("scheduleButton");
            this.hideElement("loadButton");
            this.hideElement("exportButton");
            this.hideElement("targetContainer");
            this.hideElement("sourceContainer");
            App.JobScheduleController.openSchedulerForm();
            break;
        }
      },
      checkTotalNumItems: function (num) {
        var limit = LOAD_EXPORT_SYNC_LIMIT;
        if (this.isNow()) {
          // we will hide and enable these buttons depending on whether number is less/greater than limit
          //  then we call update page where the buttons are further fine-tuned depending on async and validation
          if (num <= limit) {
            this.showElement("exportButton");
            this.enableElement("nowOption");
            return true;
          } else {
            this.hideElement("exportButton");
            this.disableElement("nowOption");
            this.showInfo("Loads or Exports greater than " + limit + " items  must be done asynchronously.");
            $("#asyncOption").prop("checked", true);
            $('input[type="radio"][name="whenOption"]').trigger("click");
            return false;
          }
        } else {
          return true;
        }
      },
      setEstimatedTotal: function (num) {
        App.PageController.estimatedTotal = num;
      },
      getEstimatedTotal: function (num) {
        return App.PageController.estimatedTotal;
      },
      paramsChanged: function () {
        // implemented in subclasses
      },
      setInvalidForLoad: function () {
        // implemented in subclasses
      },
      ProgressBar: function (title, total) {
        return new ProgressBar(title, total);
      },
      setEnd_date: function (val) {
        val = new Date(val);
        val = App.JobScheduleController.getDateStrFromDate(val);
        $('#end_date').val(val);
        /* no need to do this KM 7-26-16
         // val is an UTC date.  get the offset and convert
         var localOffset = new Date().getTimezoneOffset() * 60000;
         var secs = val.getTime() + localOffset;
         val = new Date(secs);
         */
      },
      setStart_date: function (val) {
        val = new Date(val);
        val = App.JobScheduleController.getDateStrFromDate(val);
        $('#start_date').val(val);

        /* no need to do this KM 7-26-16
         val is an UTC date.  get the offset and convert
         var localOffset = new Date().getTimezoneOffset() * 60000;
         var secs = val.getTime() + localOffset;
         val = new Date(secs);
         */
      },
      setOne_time_date: function (val) {
        val = new Date(val);
        log("New Date = " + val);
        val = App.JobScheduleController.getDateStrFromDate(val);
        log("Date String = " + val);
        $('#one_time_date').val(val);
        log("Input is set to = " + $('#one_time_date').val());

        // val is an UTC date.  get the offset and convert
        /* no need to fiddle with time for UTC KM 7-26-16
         var localOffset = new Date().getTimezoneOffset() * 60000;
         var secs = val.getTime() + localOffset;
         val = new Date(secs);
         val = App.JobScheduleController.getDateStrFromDate(val);
         $('#one_time_date').val(val);
         */
      },

      initFinalDisplayAdjust: function () {
        // implemented in subclasses
      },

      contextMenuCmd: function (cmd, node) {
        switch (cmd) {
          case '#setloadsource':
            this.setRootSourcePath(node.data.path);
            break;
          case '#setloadtarget':
            this.setTargetPath(node.data.dataid, node.data.path, true);
            break;
          case '#loadfromhere':
            var formVals;
            if (this.isUserCsv(node.data.path)) {
              formVals = { rootSourcePath: node.data.path };
              this.forwardToPage("bl", "loadusers", formVals);
            }
            else if (this.isCsvPath(node.data.path)) {
              formVals = { rootSourcePath: node.data.path };
              this.forwardToPage("bl", "loadcsv", formVals);
            }
            else if (this.isFolderPath(node.data.path)) {
              formVals = {
                "rootSourcePath": node.data.path
              };
              this.forwardToPage("bl", "loadfilestructure", formVals);
            }
            else {
              this.showError("You need to select either a CSV file or folder to load from.");
            }
            break;
          case '#setexportsource':
            if (node.data.id) {
              // it's a user
              this.setRootSourcePath(node.data.path, node.data.id);
            }
            else if (node.data.dataid) {
              this.setRootSourcePath(node.data.path, node.data.dataid);
            }
            break;
          case '#setexporttarget':
            this.setRootExportPath(node.data.path, false);
            break;
          case '#exportfromhere':
            formVals = {
              "rootSourcePath": node.data.path
            };
            this.forwardToPage("be", "exportobjects", formVals);
            break;
          case '#exportmdfromhere':
            formVals = {
              "rootSourcePath": node.data.path
            };
            this.forwardToPage("be", "exportmetadata", formVals);
            break;
          case '#skipnode':
            if (node.data.skip) {
              this.showInfo("This node is already marked for exclusion.");
            }
            else {
              App.PageController.paramsChanged();
              App.SourceTreeController.skipNode(node);
            }
            break;
          case '#includenode':
            if (node.data.skip) {
              App.PageController.paramsChanged();
              App.SourceTreeController.includeNode(node);
            }
            else {
              this.showInfo("This node is already included in the load");
            }
            break;
          case '#viewmetadata':
            this.showMetadata(node.data.path, node.data.metadata);
            break;
          case '#showfilepath':
            this.showInfo(node.data.path);
            //window.open("file://" + node.data.path);
            break;
        }
      }
    };
  });

