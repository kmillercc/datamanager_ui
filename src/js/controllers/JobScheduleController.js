define(["RootPageController"],
    function (RootPageController) {
        return function () {
            /*global App*/
            var controller = Object.create(RootPageController);

            controller.formName = "scheduleForm";

            controller.initPage = function () {
                var that = this;
                $("#schedule_type").change(function (e) {
                    that.updatePage();
                });

                $('#day_pattern').change(function (e) {
                    that.updatePage();
                });

                $('#daily_frequency').change(function (e) {
                    that.updatePage();
                });

                $(".btn_add_time").click(function (e) {
                    var id = controller.getId($(e.currentTarget).attr("id"));
                    controller.addTime(id);
                });

                $(".btn_remove_time").click(function (e) {
                    controller.removeTime(e);
                });


                $("input, select").change(function (e) {
                    controller.tryEnableScheduleButton();
                });

                $('input[type="number"]').keyup(function (e) {
                    var input = $(e.currentTarget);
                    var name = input.attr("name");
                    var val = input.val();

                    if (isNaN(parseInt(val))) {
                        controller.showError("Value must be a positive integer");
                        input.val("");
                    }
                    else {
                        if (!controller.isPositiveInteger(val)) {
                            controller.showError("Value must be a positive integer");

                            // is it a matter of just getting rid of the negative sign?
                            var negLoc = val.indexOf("-");
                            var newVal = val.substr(negLoc + 1);
                            if (negLoc !== -1 && controller.isPositiveInteger(newVal)) {
                                input.val(newVal)
                            } else {
                                input.val("");
                            }
                        }
                    }
                });


                $('#jobname').keyup(function (e) {
                    controller.tryEnableScheduleButton();
                });

                $("#scheduleButton").click(function (e) {
                    e.preventDefault();
                    controller.saveScheduledJob();
                });

                $("#btn_remove_time_1").prop("disabled", true);

                var now = new Date();

                if (controller.isNow()) {
                    var dateStr = this.getDateStrFromDate(now);
                    $('#one_time_date').val(dateStr);
                }

                if (controller.isNow()) {
                    var timeStr = this.getTimeStrFromDate(now);
                    $('#one_time_time').val(timeStr);
                }

                $("#runAsUserName")
                    // don't navigate away from the field on tab when selecting an item
                    .bind("keydown", function (event) {
                        if (event.keyCode === $.ui.keyCode.TAB &&
                            $(this).data("ui-autocomplete").menu.active) {
                            event.preventDefault();
                        }
                    })
                    .autocomplete({
                        source: function (request, response) {
                            $.when(App.ServerAPI.searchUsers(request.term))
                                .done(function (results) {
                                    response(results);
                                })
                                .fail(function (errMsg) {
                                    App.PageController.showError(errMsg);
                                });

                        },
                        minLength: 2,
                        select: function (event, ui) {
                            if (ui.item) {
                                $("#runAsUserName").val(ui.item.value);
                            }
                        },
                        open: function () {
                            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        },
                        delay: 500
                    });

                this.tryEnableScheduleButton();
                this.updatePage();

            };

            controller.addTime = function (currentCount) {
                var countInput = $("#daily_at_times_count");
                var count = parseInt(countInput.val()) + 1;
                countInput.val(count);

                var addButton = $("#btn_add_time_" + currentCount);
                var div = $("#daily_at_time_div_" + currentCount);

                var plus1 = parseInt(currentCount) + 1;
                var html = '<div id="daily_at_time_div_' + plus1 + '">' + div.html().replace(new RegExp('_' + currentCount, 'g'), '_' + plus1) + '</div>';

                div.after(html);

                // disable the add remove buttons for this element
                addButton.prop("disabled", true);
                $("#btn_remove_time_" + currentCount).prop("disabled", true);

                $("#btn_add_time_" + plus1).click(function (e2) {
                    var id = controller.getId($(e2.currentTarget).attr("id"));
                    controller.addTime(id);
                });

                $("#btn_remove_time_" + plus1)
                    .prop("disabled", false)
                    .click(function (e2) {
                        controller.removeTime(e2);
                    });


            };

            controller.removeTime = function (e) {
                var countInput = $("#daily_at_times_count");
                var count = parseInt(countInput.val()) - 1;
                countInput.val(count);
                var id = controller.getId($(e.currentTarget).attr("id"));
                var minus1 = parseInt(id) - 1;
                if ($(".btn_add_time").length === 2) {
                    // enable the add button from earlier row
                    $("#btn_add_time_1").prop("disabled", false);
                } else {
                    $("#btn_add_time_" + minus1).prop("disabled", false);
                    $("#btn_remove_time_" + minus1).prop("disabled", false);
                }

                $("#daily_at_time_div_" + id).remove();

            };

            controller.getTimeStrFromDate = function (d) {
                log("time is " + d);
                var hours = ("0" + d.getHours()).slice(-2);
                var minutes = ("0" + d.getMinutes()).slice(-2);
                log("minutes is " + minutes);
                var seconds = ("0" + d.getSeconds()).slice(-2);
                return hours + ":" + minutes + ":" + seconds;
            };

            controller.getDateStrFromDate = function (d) {
                var day = ("0" + d.getDate()).slice(-2);
                var month = ("0" + (d.getMonth() + 1)).slice(-2);
                return d.getFullYear() + "-" + (month) + "-" + (day);
            };

            controller.tryEnableScheduleButton = function () {
                if (controller.isScheduled()) {
                    if (App.PageController && App.PageController.hasRequiredFields() && controller.hasJobName()) {
                        controller.enableElement('scheduleButton');
                    }
                    else {
                        controller.disableElement('scheduleButton');
                    }
                }
            };

            controller.hasJobName = function () {
                var jobName = $('#jobname').val();
                return (jobName && jobName.length >= 3);
            };

            controller.updatePage = function () {
                var val;
                // schedule type
                val = $("#schedule_type").val();
                switch (val) {
                    case '1':
                        controller.hideElement("markAfterLoadingGroup");
                        controller.hideElement("recurringFieldset");
                        controller.hideElement("windowFieldset");
                        controller.showElement("oneTimeFieldset");
                        break;
                    case  '2':
                        if (App.PageName === "loadfilestructure" || App.PageName === "loadcsv") {
                            controller.showElement("markAfterLoadingGroup");
                        }
                        else {
                            controller.hideElement("markAfterLoadingGroup");
                        }
                        controller.hideElement("oneTimeFieldset");
                        controller.showElement("recurringFieldset");
                        break;
                }

                val = $('#day_pattern').val();
                switch (val) {
                    case '1':      // every x days
                        controller.hideElement("certainDaysGroup");
                        controller.showElement("recursEveryGroup");
                        break;
                    case  '2':     // on certain days
                        controller.hideElement("recursEveryGroup");
                        controller.showElement("certainDaysGroup");
                        break;
                }

                // daily frequency type
                val = $('#daily_frequency').val();
                switch (val) {
                    case '1':      // once during the day
                        controller.hideElement("DailyEveryXGroup");
                        controller.showElement("DailyAtGroup");
                        break;
                    case  '2':     // at regular intervals during day
                        controller.hideElement("DailyAtGroup");
                        controller.showElement("DailyEveryXGroup");
                        break;
                }

            };

            controller.openSchedulerForm = function () {
                $("#scheduleDiv").accordion({
                    collapsible: true,
                    active: true,
                    heightStyle: 'content',
                    active: 0
                });

            };


            controller.saveScheduledJob = function () {
                var type = $('#jobType').val();
                var subType = $('#jobSubtype').val();
                var optionStr = App.PageController.serializeFormVals(true);
                $.when(App.ServerAPI.saveScheduledJob(type, subType, optionStr, this))
                    .done(function (result) {
                        $("#scheduleId").val(result["scheduleId"]);
                        controller.showSuccess("The job has been saved. Monitor the status of your job in the 'Manage " + controller.initCap(type) + " Jobs page");
                    })
                    .fail(function (errMsg) {
                        controller.showError(errMsg);
                    })
            };

            controller.addDailyAddTime = function (name, val) {

                var id = controller.getId(name);
                var minus1 = parseInt(id) - 1;
                controller.addTime(minus1);
                $("#" + name).val(val);

            };

            controller.getId = function (str) {
                return str.split("_").slice(-1)[0];
            };

            return controller;

        };
    }
)
;
