define([],
    function () {
        return {
            /*global App*/
            initPage: function () {

                $("#saveProfileButton").click(function (e) {
                    e.preventDefault();
                    var selObj = $("#profileId");
                    var existingId = selObj.val();
                    var selectedOption = selObj.find(":selected");
                    var optionVal = selectedOption.text();
                    var newProfileId = $("#newProfileId");
                    var newId = newProfileId.val();
                    //log("existingId=" + existingId);
                    //log("newId=" + newId);
                    if (existingId && existingId !== "null") {
                        App.PageController.confirm("Save these form values under the '" + optionVal + "' profile?",
                            function (result) {
                                if (result) {
                                    App.ProfileController.saveProfile(function (pData) {
                                        newProfileId.val(undefined);
                                        selectedOption.attr('data', JSON.stringify(pData));
                                        App.PageController.showSuccess("Profile successfully saved.")
                                    }, null);
                                }
                            })
                    } else if (newId.length) {
                        App.PageController.confirm("Save these form values under a new profile named '" + newId + "'?",
                            function (result) {
                                if (result) {
                                    App.ProfileController.addProfile(function (pData) {
                                        selObj.append($("<option>")
                                            .val(pData.profileId)
                                            .html(pData.profileId)
                                            .attr('data', JSON.stringify(pData))
                                        );
                                        selObj.val(pData.profileId);
                                        selObj.combobox();
                                        $("#toggle").click(function () {
                                            $("#profileId").toggle()
                                        });
                                        App.PageController.showSuccess("Profile successfully saved.")
                                    });
                                }
                            })
                    }
                    else {
                        App.PageController.showError("Please enter a name for the profile in the box to the left.");
                    }

                });
            },

            profileChanged: function(){
                var selObj = $("#profileId");
                var selectedOption = selObj.find(":selected");
                var data = selectedOption.attr('data');
                if (data){
                    data = JSON.parse(data);
                    $("#newProfileId").val(undefined);
                    App.PageController.setFormVals(data);
                    App.PageController.profileChanged(data, selectedOption.val());
                }
            },

            setProfileFromInitPage: function(profileId){
                $(".custom-combobox-input").val(profileId);
            },

            saveProfile: function (callback) {
                var type = $('#jobType').val();
                var subType = $('#jobSubtype').val();
                var optionStr = App.PageController.serializeFormVals(true);
                $.when(App.ServerAPI.saveProfile(type, subType, optionStr, this))
                    .done(function (result, context) {
                        if (callback) {
                            callback(result);
                        }
                    })
                    .fail(function (errMsg, context) {
                        App.PageController.showError(errMsg);
                    })
            },
            addProfile: function (callback) {
                var type = $('#jobType').val();
                var subType = $('#jobSubtype').val();
                var optionStr = App.PageController.serializeFormVals(true);
                $.when(App.ServerAPI.addProfile(type, subType, optionStr, this))
                    .done(function (result, context) {
                        if (callback) {
                            callback(result);
                        }
                    })
                    .fail(function (errMsg, context) {
                        App.PageController.showError(errMsg);
                    })
            }
        }

    });
