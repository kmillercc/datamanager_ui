define([],
  function () {
    return {
      /*global App */
      switchPage: function (moduleName, pageName, formVals, callback) {

        // *** on switch page, make sure we clear out any real time updating from jobs page
        _.each(App.intervalIds, function (intervalId) {
          window.clearInterval(intervalId);
          App.log("cleared intervalId " + intervalId)
        });
        App.intervalIds = [];

        require([moduleName + "_lbl_" + pageName],
          function (pageContext) {
            // inactivate the active link
            $('.active').toggleClass('active');
            $('#' + moduleName + "-" + pageName).toggleClass('active');

            // empty out the outlet
            var outletDiv = $('#outletDiv');
            outletDiv.hide();
            outletDiv.html('');


            // all possible templates
            var baseTemplate = "blank";
            var pageTemplate1 = "blank";
            var pageTemplate2 = "blank";
            var pageTemplate3 = "blank";
            var pageTemplate4 = "blank";

            // page controller default to blank
            var pageController = "blank";

            // set our controller
            switch (moduleName) {

              case 'bt':
                switch (pageName) {
                  case 'managedajobs':
                    baseTemplate = 'grid_only';
                    pageController = "ManageDaJobsPage";
                    break;

                  case 'convertdropoffs':
                    baseTemplate = 'grid_with_buttons';
                    pageController = "ConvertDropoffsPage";
                    break;
                }
                break;

              case 'bl':
                switch (pageName) {
                  case 'loadfilestructure':
                    baseTemplate = 'form_with_source_target_trees';
                    pageTemplate1 = pageName;
                    pageTemplate2 = 'name_collisions';
                    pageTemplate3 = 'load_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "LoadFromFileStructurePage";
                    break;

                  case 'loadcsv':
                    baseTemplate = 'form_with_grid';
                    pageTemplate1 = pageName;
                    pageTemplate2 = 'name_collisions';
                    pageTemplate3 = 'load_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "LoadFromCsvPage";
                    break;

                  case 'generatecsv':
                    baseTemplate = 'generate_csv';
                    pageTemplate1 = pageName;
                    pageController = "GenerateCsvPage";
                    break;

                  case 'loadusers':
                    baseTemplate = 'form_with_grid';
                    pageTemplate1 = pageName;
                    pageTemplate2 = 'user_name_collisions';
                    pageTemplate3 = 'load_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "LoadUsersPage";
                    break;

                  case 'manageloadpaths':
                    baseTemplate = 'grid_only';
                    pageController = "ManageLoadPathsPage";
                    break;

                  case 'manageloadjobs':
                    baseTemplate = 'grid_only';
                    pageController = "ManageLoadJobsPage";
                    break;

                  case 'manageprofiles':
                    baseTemplate = 'grid_only';
                    pageController = "ManageLoadProfilesPage";
                    break;

                  case 'manageprivileges':
                    baseTemplate = 'grid_only';
                    pageController = "ManageLoadPrivilegesPage";
                    break;

                  case 'settings':
                    pageTemplate1 = pageName;
                    baseTemplate = 'form_only';
                    pageController = "BL_SettingsPage";
                    break;

                  case 'faq':
                    pageTemplate1 = pageName;
                    baseTemplate = 'simple_text_page';
                    pageController = "FaqPage";
                    break;

                  case 'help':
                    pageTemplate1 = pageName;
                    baseTemplate = 'simple_text_page';
                    pageController = "HelpPage";
                    break;
                }

                break;

              case 'be':
                switch (pageName) {
                  case 'exportfromdataids':
                    baseTemplate = 'form_with_source_target_trees';
                    pageTemplate1 = 'exportobjects';
                    pageTemplate2 = "export_options";
                    pageTemplate3 = 'export_advanced';
                    pageTemplate4 = 'schedule_job';

                    if (formVals["metadataOnly"]) {
                      pageController = "ExportMetadataPage";
                    }
                    else {
                      pageController = "ExportObjectsPage";
                    }
                    break;

                  case 'exportobjects':
                    baseTemplate = 'form_with_source_target_trees';
                    pageTemplate1 = pageName;
                    pageTemplate2 = "export_options";
                    pageTemplate3 = 'export_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "ExportObjectsPage";
                    break;

                  case 'exportmetadata':
                    baseTemplate = 'form_with_source_target_trees';
                    pageTemplate1 = pageName;
                    pageTemplate2 = "export_options";
                    pageTemplate3 = 'export_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "ExportMetadataPage";
                    break;

                  case 'exportusers':
                    baseTemplate = 'form_with_source_target_trees';
                    pageTemplate1 = pageName;
                    pageTemplate3 = 'export_advanced';
                    pageTemplate4 = 'schedule_job';
                    pageController = "ExportUsersGroupsPage";
                    break;

                  case 'manageexportpaths':
                    baseTemplate = 'grid_only';
                    pageController = "ManageExportPathsPage";
                    break;

                  case 'manageexportjobs':
                    baseTemplate = 'grid_only';
                    pageController = "ManageExportJobsPage";
                    break;

                  case 'manageprofiles':
                    baseTemplate = 'grid_only';
                    pageController = "ManageExportProfilesPage";
                    break;

                  case 'manageprivileges':
                    baseTemplate = 'grid_only';
                    pageController = "ManageExportPrivilegesPage";
                    break;

                  case 'settings':
                    pageTemplate1 = pageName;
                    baseTemplate = 'form_only';
                    pageController = "BE_SettingsPage";
                    break;

                  case 'faq':
                    pageTemplate1 = pageName;
                    baseTemplate = 'simple_text_page';
                    pageController = "FaqPage";
                    break;

                  case 'help':
                    pageTemplate1 = pageName;
                    baseTemplate = 'simple_text_page';
                    pageController = "HelpPage";
                    break;
                }

                break;
            }

            // set the pageName for later use
            App.PageName = pageName;

            // templates
            baseTemplate = App.TemplateUtils.getTemplate(baseTemplate);
            pageTemplate1 = App.TemplateUtils.getTemplate(moduleName + "/" + pageTemplate1);
            App.TemplateUtils.registerPartial("page_content1", pageTemplate1);
            pageTemplate2 = App.TemplateUtils.getTemplate(moduleName + "/" + pageTemplate2);
            App.TemplateUtils.registerPartial("page_content2", pageTemplate2);
            pageTemplate3 = App.TemplateUtils.getTemplate(moduleName + "/" + pageTemplate3);
            App.TemplateUtils.registerPartial("page_content3", pageTemplate3);
            pageTemplate4 = App.TemplateUtils.getTemplate(pageTemplate4);
            App.TemplateUtils.registerPartial("page_content4", pageTemplate4);
            var html = baseTemplate(pageContext);
            outletDiv.append(html);

            require([pageController],
              function (pageController) {
                App.PageController = new pageController();

                // finally initiate the page
                App.PageController.initPage(formVals, function () {
                  if (formVals) {
                    App.PageController.setFormVals(formVals);
                  }
                  App.PageController.initFinalDisplayAdjust(formVals);
                  outletDiv.show();

                  if (callback) {
                    callback();
                  }

                });

              });

          });
      }
    };

  });