define({
    "page_title": "Manage Distributed Agent Tasks",
    "help_block" : "This page shows the currently running Content Server distributed agent tasks used by Scheduled Export or Load jobs. In case of problems, you can delete a distributed agent task. Deleting the task will terminate the export or load, but the schedule will remain saved if you want to run it later.  To restart or reschedule, go to the 'Manage Async Load Jobs' or 'Manage Async Export Jobs' page.",
    "action_buttons": [
        //{id: "addButton", className: "btn btn-small btn-primary", label: "Add"}
    ],
    grid_label: ""
});