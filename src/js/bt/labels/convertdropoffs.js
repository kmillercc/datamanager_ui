define({
    "page_title": "BulkLoader Drop-off Folders",
    "help_block" : "This page allows you to convert previously created BulkLoader drop-off folders to new Data Manager Asynchronous Load Jobs",
    "action_buttons": [
        {id: "submitButton", className: "btn btn-small btn-primary btn-action", label: "Convert Selected Jobs"}
    ],
    grid_label: ""
});