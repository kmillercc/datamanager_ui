define(["RootPageController", "DropoffJobModel", "DataTableCrudController"],
    function (RootPageController, dropoffModel, DataTableCrudController) {
        return function () {
            /*global App*/
            /*global LL_URL*/

            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Dropoff Job from the BULKLOADER_WATCHED_FOLDERS table");
                App.DataTableController.setApiFuncs("datamanager.getDropoffJobs", "", "", "datamanager.deleteDropoffJob");

                // callback
                if (callback) {
                    callback();
                }

                $("#submitButton").click(function (e) {
                    controller.submitConversionJobs();
                });

                App.DataTableController.initController(dropoffModel, false, {}, false, 'id', function () {
                    $(".convertCheckbox").click(function (e) {
                        controller.updatePage();
                    });

                    $("#selectAll").click(function (e) {
                        if ($("#selectAll").prop('checked')) {
                            $(".convertCheckbox").prop('checked', true);
                        }
                        else {
                            $(".convertCheckbox").prop('checked', false);
                        }
                        controller.updatePage();
                    });

                    $(".jobTypeSelect").change(function (e) {
                        var select = $(e.currentTarget);
                        var id = select.attr("data");
                        var actionSel = $("#jobAction_" + id);
                        var val = select.val();
                        if (val === "loadfilestructure") {
                            actionSel.val("create").attr("disabled", true);
                        } else {
                            actionSel.attr("disabled", false);
                        }

                    });
                });
            };

            controller.hasRequiredFields = function () {
                return controller.areAnyChecked("convertCheckbox");
            };

            controller.submitConversionJobs = function () {

                var ok = true;
                var selectedCheckboxes = $('.convertCheckbox:checked');
                var total = selectedCheckboxes.length;
                var count = 0;
                var numFailures = 0;

                selectedCheckboxes.each(function () {
                    var id = $(this).attr("value");
                    var jobType = $("#jobType_" + id).val();
                    var jobAction = $("#jobAction_" + id).val();

                    $.when(App.ServerAPI.convertDropoffJob(id, jobType, jobAction, controller))
                        .done(function () {
                            count += 1;
                            //style="color: green; font-weight: bold"
                            $("#checkboxSpan_" + id).css("color", "green").css("font-weight", "bold").html("Successfully Converted");
                            if (count === total) {
                                controller.showOverallResult(numFailures);
                            }
                        })
                        .fail(function (errMsg) {
                            count += 1;
                            numFailures += 1;
                            $("#checkboxSpan_" + id).css("color", "red").css("font-weight", "bold").html("Error: " + errMsg);
                            ok = false;
                            if (count === total) {
                                controller.showOverallResult(numFailures);
                            }
                        });

                });
            };

            controller.showOverallResult = function (numFailures) {
                if (numFailures === 0) {
                    controller.showInfo("All selected jobs were successfully converted. To check the results and edit the jobs, go to 'Manage Async Load Jobs.'");
                } else {
                    controller.showError("Conversion failed on " + numFailures + " jobs. Check the left column for error messages.");
                }
            };


            return controller;
        };
    });
