define(["RootPageController", "DaJobModel", "DataTableCrudController"],
    function (RootPageController, daModel, DataTableCrudController) {
        return function () {
            /*global App*/
            /*global LL_URL*/

            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Distributed Agent Task");
                App.DataTableController.setApiFuncs("datamanager.getWorkerQueue", "", "", "datamanager.deleteFromWorkerQueue");

                // *** on switch page, make sure we clear out any real time updating from jobs page
                _.each(App.intervalIds, function (intervalId) {
                    window.clearInterval(intervalId);
                    App.log("cleared intervalId " + intervalId)
                });
                App.intervalIds = [];

                // callback
                if (callback) {
                    callback();
                }

                App.DataTableController.initController(daModel, false, {handlerID: 'BulkToolsJob'}, false, 'id');
            };


            return controller;
        };
    });