define({
    colNames: [],
    "dgColumnDefs": [
        {"bSortable": false, "mData": "id", "sName": "id", "aTargets": [0], "sType": "number", "bVisible": false },
        {"bSortable": false, "mData": "jobname", "sName": "jobname", "aTargets": [1], "sTitle": "Job Name", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "queuetime", "sName": "queuetime", "aTargets": [2], "sTitle": "QueueTime", "sClass": "center", "sType": "date"},
        {"bSortable": false, "mData": "workertime", "sName": "workertime", "aTargets": [3], "sTitle": "Workertime", "sClass": "center", "sType": "date"},
        {"bSortable": false, "mData": "priority", "sName": "priority", "aTargets": [4], "sTitle": "Priority", "sClass": "center", "sType": "number"},
        {"bSortable": false, "mData": "attempts", "sName": "attempts", "aTargets": [5], "sTitle": "Attempts", "sClass": "center", "sType": "number"},
        {"bSortable": false, "mData": "workerid", "sName": "workerid", "aTargets": [6], "sTitle": "WorkerID", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "fingerprint", "sName": "fingerprint", "aTargets": [7], "sTitle": "Fingerprint", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "status", "sName": "status", "aTargets": [8], "sTitle": "Status", "sClass": "center", "sType": "number"},
        {"bSortable": false, "mData": "Action", "sName": "Action", "aTargets": [9], "sTitle": "Row Actions", "sClass": "center", "sType": "html", "sDefaultContent": '&nbsp;<a href="" class="editor_remove">Delete</a>'}
    ]
});




