/* the database columns:
    "ID"             NUMBER(10,0) NOT NULL ENABLE,
    "PROCESS_SERVER" VARCHAR2(64 CHAR),
    "DIRPATH"        VARCHAR2(4000 BYTE) NOT NULL ENABLE,
    "LOAD_PATH"      VARCHAR2(4000 BYTE) NOT NULL ENABLE,
    "LOAD_ID"        NUMBER(10,0) NOT NULL ENABLE,
    "DMASK"          NUMBER(10,0),
    "HMASK"          NUMBER(10,0),
    "MMASK"          NUMBER(10,0),
    "OPTIONS" CLOB,
    "NEXTTIME" DATE,
    "FRIENDLY_NAME" VARCHAR2(255 CHAR)


 */

define({
    colNames: [],
    "dgColumnDefs": [
        {"bSortable": false, "mData": "checkbox", "sName": "checkbox", "aTargets": [0], "sType": "html", "sClass": "center", "sTitle": '<input type="checkbox" id="selectAll" name="selectAll">'},
        {"bSortable": false, "mData": "jobtype", "sName": "jobtype", "aTargets": [1], "sType": "html", "sClass": "center", "sTitle": "Job Type"},
        {"bSortable": false, "mData": "action", "sName": "action", "aTargets": [2], "sType": "html", "sClass": "center", "sTitle": "Action to Perform"},
        {"bSortable": false, "mData": "id", "sName": "id", "aTargets": [3], "sType": "number", "bVisible": false },
        {"bSortable": false, "mData": "jobName", "sName": "jobName", "aTargets": [4], "sTitle": "Job Name", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "processServer", "sName": "processServer", "aTargets": [5], "sTitle": "Process Server", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "loadPath", "sName": "loadPath", "aTargets": [6], "sTitle": "Load Path", "sClass": "center", "sType": "string"},
        {"bSortable": false, "mData": "loadId", "sName": "loadId", "aTargets": [7], "sTitle": "Load ID", "sClass": "center", "sType": "number"},
        {"bSortable": false, "mData": "options", "sName": "options", "aTargets": [8], "sTitle": "Options", "sClass": "center", "sType": "string" },
        {"bSortable": false, "mData": "Action", "sName": "Action", "aTargets": [9], "sTitle": "Action", "sClass": "center", "sType": "html", "sDefaultContent": '&nbsp;<a href="" class="editor_remove">Delete</a>'}
    ]
});




