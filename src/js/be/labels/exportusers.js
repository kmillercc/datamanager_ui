define({
    "page_title" : "Export Users/Groups",
    "action_buttons": [
        {id : "exportButton", className: "btn btn-small btn-action btn-primary", label: "Export Now"},
        {id : "scheduleButton", className: "btn btn-small btn-primary", label: "Save Scheduled Job"}
    ],
    grid_label: "",
    source_label: "Source View",
    target_label: "Target View"
});