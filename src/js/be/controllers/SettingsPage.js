define(["BulkExporterPageController"],
    function (BulkExporterPageController) {
        return function () {
            var controller = Object.create(BulkExporterPageController);
            controller.initPage = function (formVals, callback) {
                BulkExporterPageController.initPage.call(this, formVals);

                    // callback
                    if (callback){
                        callback();
                    }
            };

            return controller;
        }
    });