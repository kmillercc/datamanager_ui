define(["RootPageController", "ExportPrivilegeModel", "DataTableCrudController"],
    function (RootPageController, ExportPrivilegeModel, DataTableCrudController) {
        return function() {
            /*global App*/
            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("User");
                App.DataTableController.setApiFuncs("datamanager.getExportUsers","datamanager.addEditExportUser", "datamanager.addEditExportUser", "datamanager.removeExportUser");
                App.DataTableController.initController(ExportPrivilegeModel, true, null, true, 'userName');

                    // callback
                    if (callback){
                        callback();
                    }
            };


            return controller;
        };
   });