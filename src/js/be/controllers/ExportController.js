define(["ProcessController"],
    function (ProcessController) {
        /*global App*/
        return function () {
            var controller = Object.create(ProcessController);

            controller.exportSuccess = function (result) {
                var msg = result && result["msg"] || "Export completed successfully.";
                App.PageController.showSuccess(msg);
                App.PageController.setRootExportPath(App.PageController.rootExportDir, false);
            };

            controller.exportError = function (err) {
                App.PageController.showError(err);
            };


            controller.nonFatalError = function (result) {
                var msg = result["msg"] || "The export completed, but there were " + result.nonFatalErrors + " errors.  Errors have been logged to the '_audit' folder in the export directory.";
                App.PageController.showWarning(msg);
            };

            controller.runExport = function (optionStr) {
                // start the stopWatch
                this.stopWatch.start();

                var context = this;

                $.when(controller.initializeExport(optionStr, controller))
                    .done(function (result) {
                        App.PageController.setRootExportPath(result["rootTargetPath"], false);

                        controller.showInitializeProgress(controller.jobId, controller.exportError, controller.nonFatalError, controller.finalizeExport, "Exporting Items", context, "Exported");

                    })
                    .fail(function (errMsg) {
                        App.PageController.showError(errMsg);
                    });
            };

            controller.finalizeExport = function () {
                var rate = (controller.stopWatch.getDurationSeconds() / controller.itemsInitialized).toFixed(3);
                log ("Timings:");
                log("   " + controller.itemsInitialized + " items processed");
                log("   " + controller.stopWatch.getDurationStr() + " elapsed time");
                log("   " + rate + " seconds per item");
                controller.stopWatch.stop();

                App.PageController.hideLoadingIndicator();
                controller.exportSuccess();

            };

            return controller;
        }


    });
