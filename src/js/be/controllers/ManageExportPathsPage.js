define(["RootPageController", "ExportPathModel", "DataTableCrudController"],
    function (RootPageController, ExportPathModel, DataTableCrudController) {
        return function () {
            /*global App*/
            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Export Path");
                App.DataTableController.setApiFuncs("datamanager.getExportPaths", "datamanager.addEditExportPath", "datamanager.addEditExportPath", "datamanager.removeExportPath");
                App.DataTableController.initController(ExportPathModel, true);

                // callback
                if (callback) {
                    callback();
                }
            };


            return controller;
        };
    });