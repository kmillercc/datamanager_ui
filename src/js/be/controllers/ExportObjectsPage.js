define(["BulkExporterPageController", "TreeController", "DataTableCsvController", "ExportController", "ProfileController", "JobScheduleController"],
    function (BulkExporterPageController, TreeController, DataTableCsvController, ExportController, ProfileController, JobScheduleController) {
        /*global App*/
        /*global LL_URL*/
        return function () {

            App.DataTableController = new DataTableCsvController({noTableTools: true});
            App.SourceViewController = App.SourceTreeController = new TreeController("sourceDiv");
            App.TargetViewController = new TreeController("targetDiv");
            App.ProcessController = new ExportController();
            App.ProfileController = ProfileController;
            App.ProfileController.initPage();
            App.JobScheduleController = new JobScheduleController();
            App.JobScheduleController.initPage();

            var controller = Object.create(BulkExporterPageController);
            controller.pageType = "objects";
            controller.jobSubtype = "exportobjects";

            controller.initPage = function (formVals, callback) {
                BulkExporterPageController.initPage.call(this, formVals, callback);

            };

            return controller;
        };
    });