define(["RootPageController", "ExportProfileModel", "DataTableCrudController"],
    function (RootPageController, ExportProfileModel, DataTableCrudController) {
        return function() {
            /*global App*/
            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Export Profile");
                App.DataTableController.setApiFuncs("datamanager.getExportProfiles","datamanager.addEditExportProfile", "datamanager.addEditExportProfile", "datamanager.removeExportProfile");
                App.DataTableController.initController(ExportProfileModel, false, null, null, 'profileId');

                    // callback
                    if (callback){
                        callback();
                    }
            };


            return controller;
        };
   });