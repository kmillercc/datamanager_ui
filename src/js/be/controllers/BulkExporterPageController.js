define(["RootPageController", "BulkExporterPageController", "ExportPathModel", "ExportProfileModel", "TreeController", "customfile", "IdNameModel"],
    function (RootPageController, BulkExporterPageController, ExportPathModel, ExportProfileModel, TreeController, customFile, IdNameModel) {

        /*global App*/
        var controller = Object.create(RootPageController);
        controller.currentSourceId = null;
        controller.modalSourcePathSelected = null;
        controller.modalTargetPathSelected = null;
        controller.modalDataIdPathSelected = null;
        controller.rootExportDir = null;
        controller.actionToPerform = "exportnodes";
        controller.sourceModalTital = "Select a Root Export Folder";
        controller.dataIdModalTital = "Select a text or csv file containing a list of Content Server DataIds";
        controller.rootNode = null;
        controller.helpText = "Select a folder from the tree and then click 'Select'";
        controller.jobSubtype = "";
        controller.profile = null;

        controller.getHiddenCols = function (rawColNames) {
            return []
        };

        controller.initPage = function (formVals, callback) {
            RootPageController.initPage.call(this, formVals);

            controller.profileModel = ExportProfileModel;

            // *** make 1 call to the server to get everything we need for this page
            $.when(App.ServerAPI.getExportPageConfig(ExportPathModel, ExportProfileModel, IdNameModel, formVals, {jobSubtype: $('#jobSubtype').val()}))
                .done(function (data) {

                    // handle the paths
                    var exportPaths = data["exportPaths"];
                    if (exportPaths.length === 1) {
                        controller.setRootExportPath(exportPaths[0].path, true, false);
                    }

                    // handle profiles
                    var profiles = data["profiles"];
                    controller.setSelectOptions("profileId", "profileId", "profileId", profiles, false,
                        function () {
                            $("#profileId").combobox();
                            $("#toggle").click(function () {
                                log('clicked');
                                $("#profileId").toggle()
                            });
                        });

                    // handle object types
                    var objectTypes = data["objectTypes"];
                    controller.setSelectOptions("id", "name", "specNodeTypes", objectTypes, false);

                    // handle the html
                    var columnHtml = data["columnHtml"];
                    $("#columnsToExportDiv").append(columnHtml);
                    controller.setupCsvCols();

                    // do all the accordion elements
                    $("#optionsDiv").accordion({
                        collapsible: true,
                        active: false,
                        heightStyle: 'content'
                    });

                    // callback
                    if (callback) {
                        callback();
                    }

                })
                .fail(function (errMsg) {
                    App.PageController.showError(errMsg);
                });


            $("#exportButton").click(function (e) {
                e.preventDefault();
                App.ProcessController.runExport(App.PageController.serializeFormVals());
            });

            $('input:radio[name=exportType]').click(function (e) {
                controller.updatePage();
            });

            $('input:radio[name=sourceType]').click(function (e) {
                controller.updatePage();
            });

            $('input:radio[name=objectTypes]').click(function (e) {
                controller.updatePage();
            });

            $("#wantCategoryData").click(function (e) {
                controller.updatePage();
            });

            $('input:radio[name=metadataSelection]').click(function (e) {
                controller.updatePage();
            });

            // *** set the file input to use custom file settings
            $('input[type=file]').customFile();


            $("#selectExportPathButton").click(function (evt) {
                    App.TargetPathSelectController = new TreeController("treeDiv_select", "myModal");
                    var html = App.TemplateUtils.getTemplate(App.browseExportPathHtmlPath)({help_block: App.PageController.helpText});
                    controller.openModalWindow(html, App.PageController.sourceModalTital, true, null, null, "auto", "500px");
                    $('#selectButton').click(function (e) {
                        e.preventDefault();
                        App.PageController.targetPathSelectClicked();
                    });
                    controller.getSetSelectOptions(ExportPathModel, "path", "path", "startPath", App.ServerAPI.getExportPaths, {}, true, null);
                    $("#startPath").change(function (evt) {
                        var path = $("#startPath").val();
                        if (path.length) {
                            App.TargetPathSelectController.loadFileTreeSelect(path, false, App.PageController.targetNodeModalActivate, null);
                        }
                    });
                }
            )
            ;

            $("#selectDataIdFileButton").click(function (evt) {
                    App.TargetPathSelectController = new TreeController("treeDiv_select", "myModal");
                    var html = App.TemplateUtils.getTemplate(App.browseExportPathHtmlPath)({help_block: App.PageController.helpText});
                    controller.openModalWindow(html, App.PageController.dataIdModalTital, true, null, null, "auto", "500px");
                    $('#selectButton').click(function (e) {
                        e.preventDefault();
                        App.PageController.dataIdFilePathSelectClicked();
                    });
                    controller.getSetSelectOptions(ExportPathModel, "path", "path", "startPath", App.ServerAPI.getExportPaths, {}, true, null);
                    $("#startPath").change(function (evt) {
                        var path = $("#startPath").val();
                        if (path.length) {
                            App.TargetPathSelectController.loadFileTreeSelect(path, true, App.PageController.dataIdFileModalActivate, null);
                        }
                    });
                }
            )
            ;

            $("#selectSourcePathButton").click(function (evt) {
                controller.openModalWindow('<div id="myModal"><img class="progress-img"></div>', null, true, "modal_progress_indicator_div", false);
                App.SourcePathSelectController = new TreeController("treeDiv_select", "myModal");
                var html = App.TemplateUtils.getTemplate(App.browseSelectHtmlPath)({});
                App.SourcePathSelectController.loadLLTreeInModal(1, "System", html, "Select Source Container", false, controller.sourceNodeModalActivate, controller.sourcePathSelectClicked);
            });

            controller.hideShowAdvancedOptions();
        };

        controller.initFinalDisplayAdjust = function(){
            controller.updatePage();
        };

        controller.hideShowAdvancedOptions = function () {
            // *** initially hide all of the items
            $('div.advanced_settings > label').hide();

            // *** show the options for this pageType (either "csv" or "structure"
            $('div.advanced_settings > label.be_' + App.PageController.pageType).show();

            // *** initially hide the metadata to export stuff
            var dataToExportSubDiv = $('#dataToExportSubDiv');
            var optionsSubDiv = $('#optionsSubDiv');

            // hide label
            dataToExportSubDiv.find('> label').hide();

            // *** show the options for this pageType (either "csv" or "structure"
            dataToExportSubDiv.find('> label.be_' + App.PageController.pageType).show();

            // *** initially hide options
            optionsSubDiv.find('> div').hide();

            // *** show the options for this pageType (either "csv" or "structure"
            optionsSubDiv.find('> div.be_' + App.PageController.pageType).show();
        };

        controller.gridExportDone = function () {
            $("#grid_div").removeClass("hideBorder");
            this.showElement("grid_div");
            this.updatePage();
        };

        controller.enableActionButtons = function () {
            if (this.isNow()){
                this.enableElement("exportButton");
                this.showElement("exportButton");
            }
        };

        controller.disableActionButtons = function () {
            if (this.isNow()){
                this.disableElement("exportButton");
                this.hideElement("exportButton");
            }
        };

        controller.hasRequiredFields = function () {
            return (controller.hasRootExportPath() && (controller.hasRootSourcePath() || controller.hasDataIdFilePath()));

        };

        controller.hasRootExportPath = function(){
            var rootExportPath = $('#rootExportPath');
            return (rootExportPath.length && rootExportPath.val() && rootExportPath.val().length && controller.rootExportDir !== null);
        };

        controller.hasRootSourcePath = function(){
            var rootSourcePath = $('#rootSourcePath');
            return (rootSourcePath.length && rootSourcePath.val() && rootSourcePath.val().length);
        };

        controller.hasDataIdFilePath = function(){
            var dataIdPath = $('#dataIdFilePath');
            return (dataIdPath.length && dataIdPath.val() && dataIdPath.val().length);
        };

        controller.updatePage = function () {
            RootPageController.updatePage.call(this);
            var objectTypes = $("input:radio[name=objectTypes]:checked").val();
            if (objectTypes === 'files') {
                this.disableElement("hierarchyRadio");
                $("#flatRadio").prop("checked", true);
                $("#hierarchyRadio").prop("checked", false);
            }
            else {
                this.enableElement("hierarchyRadio");
                if (objectTypes === 'folders') {
                    this.disableElement("flatRadio");
                    $("#flatRadio").prop("checked", false);
                    $("#hierarchyRadio").prop("checked", true);
                } else {
                    this.enableElement("flatRadio");
                }

            }

            // show the select box for object types
            if (objectTypes === "specified") {
                controller.showElement("specNodeTypes");
            } else {
                controller.hideElement("specNodeTypes");
            }

            if ($("#wantCategoryData").prop("checked")) {
                this.showElement("catsToExportDiv")
            } else {
                this.hideElement("catsToExportDiv")
            }

            var exportType = $("input:radio[name=exportType]:checked").val();
            if (exportType === 'flat') {
                $("input:radio[name=numMetadataFiles]").val(['single']);
                this.disableElement("multipleCSVRadio");
            }
            else {
                this.enableElement("multipleCSVRadio");
            }

            var sourceType = $("input:radio[name=sourceType]:checked").val();
            if (sourceType === 'browse') {
                this.hideElement("dataIdFileSourceDiv");
                // km 9-12-19 blank out the dataid file path
                $('#dataIdFilePath').val("");
                this.disableElement("dataIdFileSourceDiv");
                this.enableElement("browseSourceDiv");
                this.showElement("browseSourceDiv");
                $('#jobSubtype').val(App.PageController.jobSubtype);
            }
            else if (sourceType === 'file') {
                this.hideElement("browseSourceDiv");
                this.disableElement("browseSourceDiv");
                this.showElement("dataIdFileSourceDiv");
                this.enableElement("dataIdFileSourceDiv");
                //$('#jobSubtype').val("exportfromdataids");
                $('#jobSubtype').val(App.PageController.jobSubtype);
            }
        };
        controller.sourcePathSelectClicked = function () {
            if (controller.modalSourcePathSelected) {
                controller.closeModalWindow();
                controller.setRootSourcePath(controller.modalSourcePathSelected, controller.currentSourceId);
            }
        };

        controller.targetPathSelectClicked = function () {
            if (controller.modalTargetPathSelected) {
                controller.closeModalWindow();
                controller.setRootExportPath(controller.modalTargetPathSelected, true);
            }
        };

        controller.dataIdFilePathSelectClicked = function () {
            if (controller.modalDataIdPathSelected) {
                controller.closeModalWindow();
                $('#dataIdFilePath').val(controller.modalDataIdPathSelected);
                App.PageController.updatePage();
            }
        };

        controller.setRootSourcePath = function (path, dataId) {
            // replace any escaped ampersand
            path = path.replace(";amp;", "&");
            dataId = dataId === true || dataId === false ? undefined : dataId;
            $('#rootSourceId').val(dataId);
            $('#rootSourcePath').val(path);

            var pathNames = path.trim().split(":");
            $('#subTargetPath').val(pathNames[pathNames.length - 1]);

            // *** check to make sure we are not over the limit
            $.when(App.ServerAPI.getNodeCount(path, dataId, false))
                .done(function (num) {
                    if (controller.checkTotalNumItems(num)) {
                        controller.setEstimatedTotal(num);
                        controller.updatePage();
                        controller.showLoadingIndicator("Processing");
                        App.SourceTreeController.loadLLTree(dataId, path, true, false, null, [
                            {title: "Export From Here", cmd: "setexportsource"},
                            {title: "Refresh", cmd: "setexportsource"},
                            {title: "Exclude this node from the Export", cmd: "skipnode"},
                            {title: "Include this node from the Export", cmd: "includenode"}
                        ], function () {
                            controller.hideLoadingIndicator();
                            controller.rootNode = App.SourceTreeController.getRootNode();
                        });
                    }
                })
                .fail(function (errMsg) {
                    controller.showError(errMsg);
                });

        };

        controller.setRootExportPath = function (path, updateInput, updatePage) {
            updatePage = typeof updatePage == "undefined" ? true : updatePage;
            updateInput = typeof updateInput == "undefined" ? true : updateInput;
            App.TargetViewController.loadFileTree(path, true, false, false, null, [
                {title: "Set as Target Folder", cmd: "setexporttarget"},
                {title: "Refresh", cmd: "setexporttarget"},
                {title: "Load from here", cmd: "loadfromhere"}
            ], true, function () {
                // do nothing
            });
            if (updateInput) {
                $('#rootExportPath').val(path);
            }
            controller.rootExportDir = path;
            if (updatePage) {
                controller.updatePage();
            }
        };

        controller.dataIdFileModalActivate = function (node) {
            controller.modalDataIdPathSelected = node.data.path;
            if (controller.isTextFile(node)) {
                controller.enableElement("selectButton");
            } else {
                controller.disableElement("selectButton");
            }
        };

        controller.sourceNodeModalActivate = function (node) {
            controller.enableElement("selectButton");
            controller.currentSourceId = node.data.dataid;
            controller.modalSourcePathSelected = node.data.path;
        };

        controller.targetNodeModalActivate = function (node) {
            controller.enableElement("selectButton");
            controller.modalTargetPathSelected = node.data.path;
        };

        controller.getColNames = function () {
            return $(".col_Checkbox:checkbox:checked")
                .map(function () {
                    return $(this).attr("name").substr(4);
                }).get();
        };

        controller.profileChanged = function (formVals, newProfile) {
            if (controller.profile !== newProfile) {
                controller.getHtmlAndReplace("columnsToExportDiv", App.ServerAPI.getCsvColumnHtml, formVals,
                    function () {
                        controller.setupCsvCols();
                    });
            }
            controller.profile = newProfile;
        };

        controller.setupCsvCols = function () {
            $(".columnGroups").accordion({
                collapsible: true,
                active: false,
                heightStyle: 'content'
            });

            $('input.group_Checkbox').click(function (e) {
                var thisCheckbox = $(e.currentTarget);
                var colClass = ".col_" + thisCheckbox.attr('data');
                var checked = thisCheckbox.prop('checked');
                // checked for the checkboxes
                $(colClass).prop('checked', checked);
            });

            $('input.col_Checkbox').click(function (e) {
                var thisCheckbox = $(e.currentTarget);
                var groupClass = ".group_" + thisCheckbox.attr('data');
                var colClass = ".col_" + thisCheckbox.attr('data');
                var anyChecked = false;

                $(colClass).each(function () {
                    if ($(this).prop('checked')) {
                        anyChecked = true;
                        return false;
                    }
                });

                $(groupClass).prop('checked', anyChecked);

            });

            $('input.group_Select_Checkbox').click(function (e) {
                var thisCheckbox = $(e.currentTarget);
                var optionClass = ".option_" + thisCheckbox.attr('data');
                var checked = thisCheckbox.prop('checked');
                // checked for the checkboxes
                $(optionClass).prop('selected', checked);
            });

            $('select.select_Group').change(function (e) {
                var thisSelect = $(e.currentTarget);
                var groupClass = ".group_" + thisSelect.attr('data');
                var optionClass = ".option_" + thisSelect.attr('data');
                var allSelected = true;

                $(optionClass).each(function () {
                    if (!($(this).prop('selected'))) {
                        allSelected = false;
                        return false;
                    }
                });
                $(groupClass).prop('checked', allSelected);
            });
        };

        return controller;
    })
;
