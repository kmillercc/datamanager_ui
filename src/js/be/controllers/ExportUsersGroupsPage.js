define(["BulkExporterPageController", "TreeController", "ExportController", "ProfileController", "JobScheduleController"],
    function (BulkExporterPageController, TreeController, ExportController, ProfileController, JobScheduleController) {
        /*global App*/
        /*global LL_URL*/
        return function () {

            App.SourceViewController = App.SourceTreeController = new TreeController("sourceDiv");
            App.TargetViewController = new TreeController("targetDiv");
            App.ProcessController = new ExportController();
            App.ProfileController = ProfileController;
            App.ProfileController.initPage();
            App.JobScheduleController = new JobScheduleController();
            App.JobScheduleController.initPage();


            var controller = Object.create(BulkExporterPageController);
            controller.pageType = "users";
            controller.actionToPerform = "exportusers";

            controller.initPage = function (formVals, callback) {
                BulkExporterPageController.initPage.call(this, formVals, callback);

                var selSourcePathButton = $("#selectSourcePathButton");
                selSourcePathButton.unbind();

                selSourcePathButton.click(function (evt) {
                    controller.openModalWindow('<div id="myModal"><img class="progress-img"></div>', null, true, "modal_progress_indicator_div", false);
                    App.SourcePathSelectController = new TreeController("treeDiv_select", "myModal");
                    var html = App.TemplateUtils.getTemplate(App.browseSelectHtmlPath)({});
                    App.SourcePathSelectController.loadGroupTreeInModal(-1, '', html, "Select Root Group", false, controller.sourceNodeModalActivate, controller.sourcePathSelectClicked);
                });

                $(".includeCheckbox").click(function (evt) {
                    controller.updatePage();
                });

            };

            controller.initFinalDisplayAdjust = function(){
                controller.updatePage();
            };

            controller.updatePage = function () {
                BulkExporterPageController.updatePage.call(this);
                if ($("#includeMembership").prop("checked")) {
                    $("#includeGroups").prop("checked", true);
                    $("#includeUsers").prop("checked", true);
                    $(".includeUgLabel").css("color", "gray");
                }
                else {
                    $(".includeUgLabel").css("color", "#000000");
                }
            };


            controller.sourceNodeModalActivate = function (node) {
                if (node.data.isGroup) {
                    controller.enableElement("selectButton");
                } else {
                    controller.disableElement("selectButton");
                }
                controller.currentSourceId = node.data.id;
                controller.modalSourcePathSelected = node.data.path;
            };

            controller.sourcePathSelectClicked = function () {
                if (controller.modalSourcePathSelected) {
                    controller.closeModalWindow();
                    controller.setRootSourcePath(controller.modalSourcePathSelected, controller.currentSourceId);
                }
            };

            controller.setRootSourcePath = function (path, groupId) {
                groupId = groupId === true || groupId === false ? undefined : groupId;
                $('#rootSourceId').val(groupId);
                $('#rootSourcePath').val(path);


                $('#subTargetPath').val(path.split(":").slice(-1)[0]);

                // *** check to make sure we are not over the limit
                $.when(App.ServerAPI.getUserGroupCount(groupId, false))
                    .done(function (num) {
                        if (controller.checkTotalNumItems(num)) {
                            controller.setEstimatedTotal(num);
                            controller.updatePage();
                            controller.showLoadingIndicator("Processing");
                            App.SourceTreeController.loadGroupTree(groupId, path, true, false, null, [
                                {title: "Export From Here", cmd: "setsource"},
                                {title: "Refresh", cmd: "setsource"},
                                {title: "Exclude this node from the Export", cmd: "skipnode"},
                                {title: "Include this node from the Export", cmd: "includenode"}
                            ], function () {
                                controller.hideLoadingIndicator();
                                controller.rootNode = App.SourceTreeController.getRootNode();
                            });
                        }
                    })
                    .fail(function (errMsg) {
                        controller.showError(errMsg);
                    });
            };

            return controller;
        };
    });