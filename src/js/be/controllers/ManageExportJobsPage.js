define(["RootPageController", "ExportJobModel", "DataTableCrudController"],
    function (RootPageController, ExportJobModel, DataTableCrudController) {
        return function () {
            /*global App*/
            App.DataTableController = new DataTableCrudController();

            var controller = Object.create(RootPageController);

            controller.initPage = function (formVals, callback) {
                RootPageController.initPage.call(this, formVals);
                App.DataTableController.setObjectName("Export Job");
                App.DataTableController.setApiFuncs("datamanager.getExportJobs", "datamanager.addEditExportJob", "datamanager.addEditExportJob", "datamanager.removeExportJob");

                // *** on switch page, make sure we clear out any real time updating from jobs page
                _.each(App.intervalIds, function (intervalId) {
                    window.clearInterval(intervalId);
                    App.log("cleared intervalId " + intervalId)
                });
                App.intervalIds = [];

                // callback
                if (callback) {
                    callback();
                }

                App.DataTableController.initController(ExportJobModel, false, {jobtype: 'export'}, false, 'scheduleId', controller.dataTableCallback);
            };


            return controller;
        };
    });