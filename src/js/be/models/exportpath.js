define({
    dgEditorFields: [
        {"type": "hidden", "name": "id"},
        {"type": "text", "label": "Name", "name": "name"},
        {"type": "text", "label": "Server Path", "name": "path"}
    ],
    "dgColumnDefs": [
        {"bSortable": false, "mData": "id", "sName": "id", "aTargets": [0], "sType": "string", "bVisible": false },
        {"bSortable": false, "mData": "action", "sName": "action", "aTargets": [1], "sTitle": "Action", "sClass": "center", "sType": "html", "sWidth": "20%", "sDefaultContent": '&nbsp;<a href="" class="editor_edit">Edit</a>&nbsp;&nbsp;&nbsp;<a href="" class="editor_remove">Delete</a>'},
        {"bSortable": false, "mData": "name", "sName": "name", "aTargets": [2], "sTitle": "Name", "sClass": "center", "sType": "string", "sWidth": "25%" },
        {"bSortable": false, "mData": "path", "sName": "path", "aTargets": [3], "sTitle": "Server Path", "sClass": "center", "sType": "string", "sWidth": "55%" }
    ],
    colNames: ["id", "action", "name", "path", "enabled"]
});
