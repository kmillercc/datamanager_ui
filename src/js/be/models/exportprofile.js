define({colNames: ["id"
    , "action"
    , "name"
    , "rootSourcePath"
    , "rootExportPath"
    , "exportType"
    , "enabled"
    , "exporttype"
    , "jobSubtype"
    , "numMetadataFiles"
    , "numSubLevels"
    , "objectTypes"
    , "wantCategoryData"
    , "wantClassifications"
    , "wantPerms"
    , "wantPhysObj"
    , "wantProjects"
    , "wantEmails"
    , "wantAllVersions"
    , "useObjectIdForFolderName"
    , "useObjectIdForFileName"
    , "documentFilterDate"
    , "maxNameLength"],

    "dgColumnDefs": [
        {"bSortable": false, "mData": "Action", "sName": "Action", "aTargets": [0], "sTitle": "Action", "sClass": "center", "sType": "html", "sWidth": "15%", "sDefaultContent": '&nbsp;<a href="" class="editor_editprofile">Edit</a>&nbsp;&nbsp;&nbsp;<a href="" class="editor_remove">Delete</a>'},
        {"bSortable": false, "mData": "profileId", "sName": "profileId", "aTargets": [1], "sTitle": "Name", "sClass": "center", "sType": "string", "sWidth": "20%" },
        {"bSortable": false, "mData": "jobSubtype", "sName": "jobSubtype", "aTargets": [2], "sTitle": "Export Type", "sClass": "center", "sType": "string", "sWidth": "15%" },
        {"bSortable": false, "mData": "exportType", "sName": "exportType", "aTargets": [3], "sTitle": "Flat or Hierarchy", "sClass": "center", "sType": "string", "sWidth": "15%" },
        {"bSortable": false, "mData": "rootSourcePath", "sName": "rootSourcePath", "aTargets": [4], "sTitle": "Source Path", "sClass": "center", "sType": "string", "sWidth": "15%" },
        {"bSortable": false, "mData": "rootExportPath", "sName": "rootExportPath", "aTargets": [5], "sTitle": "Target Path", "sClass": "center", "sType": "string", "sWidth": "15%" }
    ]
});